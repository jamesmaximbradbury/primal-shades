//----------------------------------------------------------------------
// Project:
// By James Bradbury of Primal Visions
//----------------------------------------------------------------------
package audio.graphics.shaders;

import java.nio.FloatBuffer;
import java.lang.Math;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;
import audio.graphics.shaders.utils.Mesh;
import audio.graphics.shaders.utils.Object3D;
import audio.graphics.shaders.utils.RawResourceReader;
import audio.graphics.shaders.utils.ShaderHelper;
import audio.graphics.shaders.utils.TextureHelper;

//----------------------------------------------------------------------
// @author maxamillion
// 
//----------------------------------------------------------------------
public class BarAmpGraphPlugin extends PluginBase
{
	//-----------------------------------------------------------------------------
		private static final String TAG = "Cube Plugin";
		
		private  int mProgram;
		private  int maPositionHandle;
		private  int mAveragedSamplesHandle;
		private  int mAmpHandle;
		private  int mColorHandle;
		private  int mMVPMatrixHandle;
		private  int mMVMatrixHandle;
		
		/** Size of the texture coordinate data in elements. */
		private final static int mTextureCoordinateDataSize = 2;

		private static final float normalisedScaleFactor = 5.0f;
		/** This will be used to pass in model texture coordinate information. */
		private  int mTextureCoordinateHandle;
		/** This will be used to pass in the texture. */
		private  int mTextureUniformHandle;  
		/** This is a handle to our texture data. */
		private  int mTextureDataHandle;
		private int runOnce = -1;
		private  short[] indices;
		private  float mNormalisedAverage;
		private  FloatBuffer quadVB;
		private  int mCubeBufferIdx;
		
		//public float mAngleX;
		//public float mAngleY;
		//----------------------------	
		public  int frameBufferID;
		private int textureBufferID;
		// The objects
		private Object3D[] objectStructures = new Object3D[3];
		private  KaleidoscopeEffect kaleido;
		private  WaveEffect wave;
//-----------------------------------------------------------------------------
//----------------------------------------------------------------------
// @param context
//----------------------------------------------------------------------
public BarAmpGraphPlugin(Context context)
{
	super(context);
	
	final String vertexShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.bar_amp_vs);   		
	final String fragmentShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.bar_amp_fs);			
	
	final int vertexShaderHandle = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);		
	final int fragmentShaderHandle = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);		
	
	mProgram = ShaderHelper.createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle,new String[] {"a_Position", "a_TexCoordinate"}); 

	// get handle to the vertex shader's member
    maPositionHandle = GLES20.glGetAttribLocation(mProgram, "a_Position");
    mTextureCoordinateHandle = GLES20.glGetAttribLocation(mProgram, "a_TexCoordinate");
    mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_MVPMatrix");
    mMVMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_MVMatrix"); 
    mAmpHandle = GLES20.glGetUniformLocation(mProgram, "u_Amp");
    // get handle to fragment shader's  member
    mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
    mTextureUniformHandle = GLES20.glGetUniformLocation(mProgram, "u_Texture");
    mAveragedSamplesHandle = GLES20.glGetUniformLocation(mProgram, "u_averagedSamples");
    
    textureBufferID = createTargetTexture();
    frameBufferID =  createFrameBuffer(textureBufferID);
    
  //  kaleido = new KaleidoscopeEffect(mContext);
    wave = new WaveEffect(mContext);
}
//-----------------------------------------------------------------------------
	/**
	 * @param 
	 */
//-----------------------------------------------------------------------------
	public void addFloatVertices()
	{
		try 
	    {
	//    	int[] normalMapTextures = {R.drawable.greeneye128};
	    //	int[] normalMapTextures = {R.drawable.redcircles128};
	    	int[] normalMapTextures = {R.drawable.purpleice256};
	    	objectStructures[0] = new Object3D(normalMapTextures, R.raw.texturedcube, true, mContext);
	    	mTextureDataHandle = TextureHelper.loadTexture(mContext, objectStructures[0].getTexFile()[0]);
	    	
	    }   catch (Exception e)
		{
		//showAlert("" + e.getMessage());
	    	Log.w(TAG,"Failed to load mesh or texture");
		};
		
		// Get buffers from mesh

		Mesh mesh = objectStructures[0].getMesh();
		indices = mesh.get_indices();

	    quadVB = mesh.get_vb();
	    
	    final int buffers[] = new int[1];
		GLES20.glGenBuffers(1, buffers, 0);	
		
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER,quadVB.capacity()* FLOAT_SIZE_BYTES,quadVB, GLES20.GL_STATIC_DRAW); 
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		
		mCubeBufferIdx = buffers[0];
		
		quadVB.limit(0);
		quadVB = null;
		
	}
//-----------------------------------------------------------------------------
		/**
		 * @param 
		 */
//-----------------------------------------------------------------------------
	public void useAndSendGLSL(float averagedNormalised)
	{
		// Add program to OpenGL environment
	    GLES20.glUseProgram(mProgram);
	    
	    float[] color = {  0.0f, 1.0f, 0.0f, 1.0f};
		// Set color for drawing the triangle-
	    GLES20.glUniform4fv(mColorHandle, 1, color , 0);
	   
	    mNormalisedAverage = averagedNormalised;
	    //if(averagedNormalised < )
	    averagedNormalised = mNormalisedAverage*normalisedScaleFactor;	
	    
	    float ampModulate = averagedNormalised/2.0f;
	
	    GLES20.glUniform1f(mAveragedSamplesHandle, averagedNormalised);
	    GLES20.glUniform1f(mAmpHandle, ampModulate);
	    
	    //Log.i("averaged normals ", String.valueOf(averagedNormalised) );
	   
	}
//-----------------------------------------------------------------------------
			/**
			 * @param 
			 */
//-----------------------------------------------------------------------------
	public void buildMatrices(float mRotation, float mXposition, float mYposition)
	{
		  // Set the view matrix. This matrix can be said to represent the camera position.
		   Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
		  // leftRatio rightRatio
		   Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
				
		   boolean beatMove = true;
		   float scaledBeat = 0;
		   float scaleFactor = 2.0f;
		   float scaledAngle = 0;
		   float scaledAngleY = 0;
		   float angleFactor = -90.0f;
		   float angleFactorY = 0.0f;
		   float newXpos = mXposition;
		   float newYpos = mYposition;
		   float newRotation = mRotation;
		   
		   if(beatMove)
		   {
			   scaledBeat = mNormalisedAverage * scaleFactor;
			  // scaledAngle  = mNormalisedAverage * angleFactor;
			  // scaledAngleY  = mNormalisedAverage * angleFactorY;
		   }
		   Matrix.setIdentityM(mModelMatrix, 0);
	
 		   Matrix.translateM(mModelMatrix, 0, newXpos, newYpos, -4.0f);
		//   Matrix.translateM(mModelMatrix, 0, newXpos, newYpos, -8.0f);
		   //Matrix.rotateM(mModelMatrix, 0, 90.0f, 0.0f, 1.0f, 0.0f);
		
		   Matrix.rotateM(mModelMatrix, 0, newRotation, 1.0f, 0.0f, 0.0f);
		  // Matrix.rotateM(mModelMatrix, 0, mAngleY +scaledAngleY, 1.0f, 0.0f, 0.0f);
		  
		   //Log.d("X: ", String.valueOf(this.mAngleX ));		
			
		   // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
		   // (which currently contains model * view).
		   Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);   
			   
		   // Pass in the model view matrix.
		   GLES20.glUniformMatrix4fv(mMVMatrixHandle, 1, false, mMVPMatrix, 0);                
			   
		   // This multiplies the model view matrix by the projection matrix, and stores the result in the MVP matrix
		   // (which now contains model * view * projection).        
		   Matrix.multiplyMM(mTemporaryMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
		   System.arraycopy(mTemporaryMatrix, 0, mMVPMatrix, 0, 16);
			
		   // Pass in the combined matrix.
		   GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);		
	}
//-----------------------------------------------------------------------------
					/**
					 * @param 
					 */
//-----------------------------------------------------------------------------	
	public void drawArray(float averagedNormalised)
	{
		 // Set the active texture unit to texture unit 0.
		   GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		   
		   //---------------------------------
		   GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frameBufferID);	   
		   GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
		   
		   useAndSendGLSL(averagedNormalised);
		   int scaleFactor = (int)(averagedNormalised*15);
		   int numBars = 6;//scaleFactor;
			float posBars = -4.25f; //-7 
			float posYBars = -1.0f;
			float offset = 1.2f;
			float rotation =0.0f;
			
			//  GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, barAmp.frameBufferID);	
			//  GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
			//  barAmp.useAndSendGLSL(averagedNormaliseBeat);
			for (int x =0;x<numBars; x++)
			{
				posBars = posBars + offset ;
				buildMatrices(rotation,posBars,posYBars);
				drawVBO();	
			}
			
			rotation =180.0f;
			posYBars = 1.0f;
			posBars = -4.25f;
			
			for (int x =0;x<numBars; x++)
			{
				posBars = posBars + offset ;
				buildMatrices(rotation,posBars,posYBars);
				drawVBO();		
			}
		   
		   GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
			
			//---------------------------------
			//draw on screen 
			float[] rotateAngleAxisXYZ = new float[]{270.0f, 0.0f,0.0f,1.0f};
			float[] translate = new float[]{0.0f,0.0f,-4.5f};
		//	drawPassThruShader(translate,rotateAngleAxisXYZ); 
			//kaleido.drawKaleidoscopeShader(textureBufferID, mQuadBufferIdx, averagedNormalised);
			wave.drawWaveShader(textureBufferID, mQuadBufferIdx, averagedNormalised);
			//---------------------------------					
	}
//-----------------------------------------------------------------------------	
	private void drawVBO()
	{
		 
		   
		   // Bind the texture to this unit.
		   GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle);
			
		   // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
		   GLES20.glUniform1i(mTextureUniformHandle, 0);
		   
		   // Prepare the  data
		   
		   GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mCubeBufferIdx);
		   GLES20.glEnableVertexAttribArray(maPositionHandle);
		   GLES20.glVertexAttribPointer(maPositionHandle, 3, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, 0);
		
		   
		   GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mCubeBufferIdx);
		   GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);
		   GLES20.glVertexAttribPointer(mTextureCoordinateHandle, mTextureCoordinateDataSize, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, TRIANGLE_VERTICES_DATA_TEX_OFFSET* FLOAT_SIZE_BYTES);

		   	// Clear the currently bound buffer (so future OpenGL calls do not use this buffer).
			GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
			
			GLES20.glDrawArrays(GLES20.GL_TRIANGLES , 0, indices.length);
			checkGlError("glDrawArrays");
			
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,0);
	
	}
}
