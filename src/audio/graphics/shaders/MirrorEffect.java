//----------------------------------------------------------------------
// Project:
// By James Bradbury of Primal Visions
//----------------------------------------------------------------------
package audio.graphics.shaders;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import audio.graphics.shaders.utils.RawResourceReader;
import audio.graphics.shaders.utils.ShaderHelper;

//----------------------------------------------------------------------
// @author maxamillion
// 
//----------------------------------------------------------------------
public class MirrorEffect extends EffectBase
{
	private final String TAG = "Mirror Effect";
	private int mTexDimHandleEffect;   		// 1,1
	private int mOriginHandleEffect; // 0,0
	private int mRotationAngleHandleEffect;
	private int mRotationAnchorHandleEffect;	
	private int mZAnchorHandleEffect;
	private int mZoomHandleEffect;
	private int mZoomMirrorHandleEffect;
	
	private float averagedBeatSamples;
	//----------------------------------------------------------------------
	// @param context
	//----------------------------------------------------------------------
	public MirrorEffect(Context context)
	{
		super(context);
		final String vertexShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.pass_thru_vs);   		
	 	final String fragmentShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.mirror_fs);			
			
		final int vertexShaderHandle = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);		
		final int fragmentShaderHandle = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);		
			
		mProgramEffect = ShaderHelper.createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle,new String[] {"a_Position","a_TexCoordinate"}); 
			
		// get handle to the vertex shader's member
	    maPositionHandleEffect = GLES20.glGetAttribLocation(mProgramEffect, "a_Position");
	    mTextureCoordinateHandleEffect = GLES20.glGetAttribLocation(mProgramEffect, "a_TexCoordinate");
	    mMVPMatrixHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_MVPMatrix");
	    mMVMatrixHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_MVMatrix"); 
	    // get handle to fragment shader's  member
	    //mColorHandle = GLES20.glGetUniformLocation(mProgramEffect, "vColor");
	    mTextureUniformHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_Texture");
	    mTexDimHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_texdim0");
	    mOriginHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_origin");
	 	
	    mRotationAngleHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_rotationAngle");
	    mRotationAnchorHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_rotationAnchor");
	  
	    mZAnchorHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_zAnchor");
	    
	    mZoomHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_zoom");
	    mZoomMirrorHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_zoomMirror");

		checkGlError("kaleid constructor");	
	}
//------------------------------------------------------------------------------------	
	private void useAndSendGLSL()
	{
		float[] origin = {  2.0f, 0.0f};
		float rotation =  0.0f;
		float[] rotationAnchor = {  0.0f, 0.0f}; 
		float[] zAnchor = {  1.0f, 1.0f}; 
		float Zoom =  1.0f;	// 0.05f/averagedBeatSamples; //0.005
		float ZoomMirror = 1.0f; 
		
		GLES20.glUseProgram(mProgramEffect);
		
		GLES20.glUniform1i(mTextureUniformHandleEffect, 0);
		 
		//GLES20.glUniform2f(mTexDimHandleEffect, width, height);
		GLES20.glUniform2f(mTexDimHandleEffect, 1.0f, 1.0f); 			// 1,1
		GLES20.glUniform2fv(mOriginHandleEffect, 1, origin, 0); 		// 2,0
		
		GLES20.glUniform1f(mRotationAngleHandleEffect, rotation); 	// 0
		GLES20.glUniform2fv(mRotationAnchorHandleEffect, 1, rotationAnchor, 0); // 0,0
		
		GLES20.glUniform2fv(mZAnchorHandleEffect, 1, zAnchor, 0); 		// 1,1
		
		GLES20.glUniform1f(mZoomHandleEffect, Zoom);  				// 1
		GLES20.glUniform1f(mZoomMirrorHandleEffect, ZoomMirror); 		// 1 
		
		
	}
//------------------------------------------------------------------------------------
		public void drawMirrorShader(int fboTextureIdx, int vertexBufferIdx, float beatAveraged)
		{ 
			averagedBeatSamples = beatAveraged;
			//mQuadTextureBufferIdx = fboTextureIdx;
			//----------------------------------------------
		//	GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			//pass frame buffer to shader and draw screen
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, fboTextureIdx);
				
			useAndSendGLSL();
			
			buildEffectMatrices();
		    				 
		    GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vertexBufferIdx);
		    GLES20.glEnableVertexAttribArray(maPositionHandleEffect);
		    GLES20.glVertexAttribPointer(maPositionHandleEffect, 3, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, 0);
		  
		    GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vertexBufferIdx);
		    GLES20.glEnableVertexAttribArray(mTextureCoordinateHandleEffect);
		    GLES20.glVertexAttribPointer(mTextureCoordinateHandleEffect, 2, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, TRIANGLE_VERTICES_DATA_TEX_OFFSET* FLOAT_SIZE_BYTES);
		    	
		    GLES20.glDrawArrays(GLES20.GL_TRIANGLES , 0,36);

			checkGlError("glDrawArrays");
			
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
			GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		}
//------------------------------------------------------------------------------------
		private void buildEffectMatrices()
		{ 
			// Set the view matrix. This matrix can be said to represent the camera position.
			Matrix.setLookAtM(mViewMatrixEffect, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
			
			Matrix.frustumM(mProjectionMatrixEffect, 0, left, right, bottom, top, near, far);
			
			Matrix.setIdentityM(mModelMatrixEffect, 0);
			Matrix.translateM(mModelMatrixEffect, 0, 0.0f, 0.0f, -4.5f);
			
			Matrix.rotateM(mModelMatrixEffect, 0, 270.0f, 0.0f, 0.0f, 1.0f);
		   // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
		   // (which currently contains model * view).
		   Matrix.multiplyMM(mMVPMatrixEffect, 0, mViewMatrixEffect, 0, mModelMatrixEffect, 0);   
			   
		   // Pass in the model view matrix.
		   GLES20.glUniformMatrix4fv(mMVMatrixHandleEffect, 1, false, mMVPMatrixEffect, 0);                
			   
		   // This multiplies the model view matrix by the projection matrix, and stores the result in the MVP matrix
		   // (which now contains model * view * projection).        
		   Matrix.multiplyMM(mTemporaryMatrixEffect, 0, mProjectionMatrixEffect, 0, mMVPMatrixEffect, 0);
		   System.arraycopy(mTemporaryMatrixEffect, 0, mMVPMatrixEffect, 0, 16);
			
		   // Pass in the combined matrix.
		   GLES20.glUniformMatrix4fv(mMVPMatrixHandleEffect, 1, false, mMVPMatrixEffect, 0);
		   //checkGlError("buildFrameBufferMatrices");	
		}			
//------------------------------------------------------------------------------------
	
}
