/**
 * 
 */
package audio.graphics.shaders;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;
import audio.graphics.shaders.utils.Mesh;
import audio.graphics.shaders.utils.Object3D;
import audio.graphics.shaders.utils.RawResourceReader;
import audio.graphics.shaders.utils.ShaderHelper;
import audio.graphics.shaders.utils.TextureHelper;
//-----------------------------------------------------------------------------
/**
 * @author maxamillion
 *
 */
public class CubePlugin extends PluginBase {
//-----------------------------------------------------------------------------
	private static final String TAG = "Cube Plugin";
	
	private  int mProgram;
	private  int maPositionHandle;
	private  int mAveragedSamplesHandle;
	private  int mColorHandle;
	private  int mMVPMatrixHandle;
	private  int mMVMatrixHandle;
	private  FloatBuffer quadVB;
	/** Size of the texture coordinate data in elements. */
	private final static int mTextureCoordinateDataSize = 2;

	private  float normalisedScaleFactor = 10.0f;
	/** This will be used to pass in model texture coordinate information. */
	private  int mTextureCoordinateHandle;
	/** This will be used to pass in the texture. */
	private  int mTextureUniformHandle;  
	/** This is a handle to our texture data. */
	private  int mTextureDataHandle;
	private int runOnce = -1;
	private  short[] indices;
	private  float mNormalisedAverage;

	private  int mCubeBufferIdx;
	
	//public float mAngleX;
	//public float mAngleY;
	
	// The objects
	private Object3D[] objectStructures = new Object3D[3];
	
//-----------------------------------------------------------------------------
	/**
	 * @param context
	 */
//-----------------------------------------------------------------------------
	public CubePlugin(Context context) {
		super(context);

		final String vertexShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.cube_vs);   		
 		final String fragmentShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.cube_fs);			
		
		final int vertexShaderHandle = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);		
		final int fragmentShaderHandle = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);		
		
		mProgram = ShaderHelper.createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle,new String[] {"a_Position", "a_TexCoordinate"}); 
	
		// get handle to the vertex shader's member
        maPositionHandle = GLES20.glGetAttribLocation(mProgram, "a_Position");
        mTextureCoordinateHandle = GLES20.glGetAttribLocation(mProgram, "a_TexCoordinate");
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_MVPMatrix");
        mMVMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_MVMatrix"); 
        // get handle to fragment shader's  member
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        mTextureUniformHandle = GLES20.glGetUniformLocation(mProgram, "u_Texture");
        mAveragedSamplesHandle = GLES20.glGetUniformLocation(mProgram, "u_averagedSamples");

	}
//-----------------------------------------------------------------------------
	/*public CubePlugin(byte[] vertices) {
		super(vertices);

	}*/
//-----------------------------------------------------------------------------
	/**
	 * @param 
	 */
//-----------------------------------------------------------------------------
	public void addFloatVertices()
	{
		try 
	    {
	    	int[] normalMapTextures = {R.drawable.redcircles128};
	    	objectStructures[0] = new Object3D(normalMapTextures, R.raw.texturedcube, true, mContext);
	    	mTextureDataHandle = TextureHelper.loadTexture(mContext, objectStructures[0].getTexFile()[0]);
	    	
	    }   catch (Exception e)
		{
		//showAlert("" + e.getMessage());
	    	Log.w(TAG,"Failed to load mesh or texture");
		};
		
		// Get buffers from mesh

		Mesh mesh = objectStructures[0].getMesh();
		indices = mesh.get_indices();

	    quadVB = mesh.get_vb();
		
	  
	    
	    final int buffers[] = new int[1];
		GLES20.glGenBuffers(1, buffers, 0);	
		
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER,quadVB.capacity()* FLOAT_SIZE_BYTES,quadVB, GLES20.GL_STATIC_DRAW); 
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		
		mCubeBufferIdx = buffers[0];
		
		quadVB.limit(0);
		quadVB = null;
		
	}
//-----------------------------------------------------------------------------
		/**
		 * @param 
		 */
//-----------------------------------------------------------------------------
	public void useAndSendGLSL(float averagedNormalised)
	{
		// Add program to OpenGL environment
	    GLES20.glUseProgram(mProgram);
	    
	    float[] color = {  0.0f, 1.0f, 0.0f, 1.0f};
		// Set color for drawing the triangle-
	    GLES20.glUniform4fv(mColorHandle, 1, color , 0);
	   
	    mNormalisedAverage = averagedNormalised;
	    //if(averagedNormalised < )
	    averagedNormalised = mNormalisedAverage*normalisedScaleFactor;	
	    	
	/*	if(averagedNormalised<=0.06)
			averagedNormalised = averagedNormalised /2.0f-0.1f;
		if(averagedNormalised<=0.03)
			averagedNormalised = averagedNormalised /2.0f -0.2f;
		*/
	    GLES20.glUniform1f(mAveragedSamplesHandle, averagedNormalised);
	    //Log.i("averaged normals ", String.valueOf(averagedNormalised) );
	   
	}
//-----------------------------------------------------------------------------
			/**
			 * @param 
			 */
//-----------------------------------------------------------------------------
	public void buildMatrices(float scaleZ, float mAngleX, float mAngleY)
	{
		  // Set the view matrix. This matrix can be said to represent the camera position.
		Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
				
		Matrix.frustumM(mProjectionMatrix, 0, leftRatio, rightRatio, bottom, top, near, far);
			
	   boolean beatMove = true;
	   float scaledBeat = 0;
	   float scaleFactorZ = 10.0f;
	   float scaledAngle = 0;
	   float scaledAngleY = 0;
	   float angleFactor = -90.0f;
	   float angleFactorY = 0.0f;
	   
	   if(beatMove)
	   {
		   /*if (mNormalisedAverage<0.85)
		   {
			   scaledBeat = mNormalisedAverage * (- scaleFactorZ*10.0f);
			   scaledAngle  = mNormalisedAverage *(- angleFactor);	
			   scaledAngleY  = mNormalisedAverage *(- angleFactorY)	;
		   }*/
		   scaledBeat = mNormalisedAverage * scaleFactorZ;
		   scaledAngle  = mNormalisedAverage * angleFactor;
		   scaledAngleY  = mNormalisedAverage * angleFactorY;
	   }
	   Matrix.setIdentityM(mModelMatrix, 0);
	   Matrix.translateM(mModelMatrix, 0, 0.0f, 0.0f, scaleZ+scaledBeat);
	   // Log.d("X: ", String.valueOf(scaleZ));
	   Matrix.rotateM(mModelMatrix, 0, 45.0f, 0.0f, 1.0f, 0.0f);
	   Matrix.rotateM(mModelMatrix, 0, mAngleX , 0.0f, 1.0f, 0.0f);
	   Matrix.rotateM(mModelMatrix, 0, mAngleY , 1.0f, 0.0f, 0.0f);
	  
	   //Log.d("X: ", String.valueOf(this.mAngleX ));		
		
	   // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
	   // (which currently contains model * view).
	   Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);   
		   
	   // Pass in the model view matrix.
	   GLES20.glUniformMatrix4fv(mMVMatrixHandle, 1, false, mMVPMatrix, 0);                
		   
	   // This multiplies the model view matrix by the projection matrix, and stores the result in the MVP matrix
	   // (which now contains model * view * projection).        
	   Matrix.multiplyMM(mTemporaryMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
	   System.arraycopy(mTemporaryMatrix, 0, mMVPMatrix, 0, 16);
		
	   // Pass in the combined matrix.
	   GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);		
	}
//-----------------------------------------------------------------------------
			/**
			 * @param 
			 */
//-----------------------------------------------------------------------------	
	public void drawArray()
	{
		 // Set the active texture unit to texture unit 0.
		   GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			   
		   // Bind the texture to this unit.
		   GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle);
			
		   // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
		   GLES20.glUniform1i(mTextureUniformHandle, 0);
		   
		   // Prepare the  data
		  /* quadVB.position(0);
		   GLES20.glVertexAttribPointer(maPositionHandle, 3, GLES20.GL_FLOAT, false,TRIANGLE_VERTICES_DATA_STRIDE_BYTES, quadVB);
		   GLES20.glEnableVertexAttribArray(maPositionHandle);

		   // Pass in the texture coordinate information
		   quadVB.position(TRIANGLE_VERTICES_DATA_TEX_OFFSET);
		   GLES20.glVertexAttribPointer(mTextureCoordinateHandle, mTextureCoordinateDataSize, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, quadVB);
		   GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle); 
		  
		   // Draw the triangle
		   GLES20.glDrawArrays(GLES20.GL_TRIANGLES , 0, indices.length);
		   checkGlError("glDrawArrays");
		   
		   GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,0);
		   */
		   /*
		    * GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mCubeBufferIdx);
			GLES20.glEnableVertexAttribArray(mPositionHandle);
			mGlEs20.glVertexAttribPointer(mPositionHandle, POSITION_DATA_SIZE, GLES20.GL_FLOAT, false, stride, 0);

		    */
		   
		   GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mCubeBufferIdx);
		   GLES20.glEnableVertexAttribArray(maPositionHandle);
		   GLES20.glVertexAttribPointer(maPositionHandle, 3, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, 0);
		   
		   /*
		    GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mCubeBufferIdx);
			GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);
			mGlEs20.glVertexAttribPointer(mTextureCoordinateHandle, TEXTURE_COORDINATE_DATA_SIZE, GLES20.GL_FLOAT, false,stride, (POSITION_DATA_SIZE + NORMAL_DATA_SIZE) * BYTES_PER_FLOAT); 
		    */
		   
		   GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mCubeBufferIdx);
		   GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);
		   GLES20.glVertexAttribPointer(mTextureCoordinateHandle, mTextureCoordinateDataSize, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, TRIANGLE_VERTICES_DATA_TEX_OFFSET* FLOAT_SIZE_BYTES);
		   
		   /*
		    // Clear the currently bound buffer (so future OpenGL calls do not use this buffer).
			GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

			// Draw the cubes.
			GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, mActualCubeFactor * mActualCubeFactor * mActualCubeFactor * 36);
		    */
		   
		   	// Clear the currently bound buffer (so future OpenGL calls do not use this buffer).
			GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
			
			GLES20.glDrawArrays(GLES20.GL_TRIANGLES , 0, indices.length);
			checkGlError("glDrawArrays");
			
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,0);
					
	}
	
}
