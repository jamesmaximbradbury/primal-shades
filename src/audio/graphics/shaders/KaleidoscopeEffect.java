//----------------------------------------------------------------------
// Project:
// By James Bradbury of Primal Visions
//----------------------------------------------------------------------
package audio.graphics.shaders;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;
import audio.graphics.shaders.utils.Mesh;
import audio.graphics.shaders.utils.Object3D;
import audio.graphics.shaders.utils.RawResourceReader;
import audio.graphics.shaders.utils.ShaderHelper;

//----------------------------------------------------------------------
// @author maxamillion
// 
//----------------------------------------------------------------------
public class KaleidoscopeEffect extends EffectBase
{	
	private final String TAG = "Kaleidoscope Effect";
	int mTexDimHandleEffect;
	int mScaleHandleEffect;
	int mOffsetHandleEffect;
	int mOriginHandleEffect;
	int mDivisionsHandleEffect;
	int mDivAmountHandleEffect;
	float averagedBeatSamples;
	//----------------------------------------------------------------------
	// @param context
	//----------------------------------------------------------------------
	public KaleidoscopeEffect(Context context)
	{
		super(context);
		
		final String vertexShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.pass_thru_vs);   		
	 	final String fragmentShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.kaleidoscope_fs);			
			
		final int vertexShaderHandle = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);		
		final int fragmentShaderHandle = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);		
			
		mProgramEffect = ShaderHelper.createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle,new String[] {"a_Position","a_TexCoordinate"}); 
			
		// get handle to the vertex shader's member
	    maPositionHandleEffect = GLES20.glGetAttribLocation(mProgramEffect, "a_Position");
	    mTextureCoordinateHandleEffect = GLES20.glGetAttribLocation(mProgramEffect, "a_TexCoordinate");
	    mMVPMatrixHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_MVPMatrix");
	    mMVMatrixHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_MVMatrix"); 
	    // get handle to fragment shader's  member
	    //mColorHandle = GLES20.glGetUniformLocation(mProgramEffect, "vColor");
	    mTextureUniformHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_Texture");
	    mTexDimHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_texdim0");
	    mScaleHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_scale");
	    mOffsetHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_offset");
	    mOriginHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_origin");
	    mDivisionsHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_div");
	    mDivAmountHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_divamount");
	    
	    //might not be needed---------------------------------
	/*    final int buffers[] = new int[1];
		GLES20.glGenBuffers(1, buffers, 0);	
		
		try 
		{
			objectStructures[0] = new Object3D(R.raw.texturedcube, false, mContext);
		}   catch (Exception e)
		{
		//showAlert("" + e.getMessage());
			Log.w(TAG,"Failed to load mesh");
		};
		
		// Get buffers from mesh
		Mesh mesh = objectStructures[0].getMesh();
		quadVB = mesh.get_vb();

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER,quadVB.capacity()* FLOAT_SIZE_BYTES,quadVB, GLES20.GL_STATIC_DRAW); 
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		mQuadBufferIdx = buffers[0];
		//mQuadTextureBufferIdx = texture; //textureBufferID
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		
		quadVB.limit(0);
		quadVB = null;
		*/
		checkGlError("kaleid constructor");	
		//return mQuadBufferIdx;	
	}
//------------------------------------------------------------------------------------	
	private void useAndSendGLSL()
	{
		float[] scale = {  1.0f, 1.0f}; 
		float[] offset = {  0.0f, 0.0f}; 
		float[] origin = {  0.0f, 0.0f}; 
		//float divisions =  3.01f; 
		float divisions =  10.0f; //0.01
		//float divAmount =   6.5f/averagedBeatSamples;
		float divAmount =   0.05f/averagedBeatSamples; //0.005
	
		GLES20.glUseProgram(mProgramEffect);
		
		GLES20.glUniform1i(mTextureUniformHandleEffect, 0);
		 
		//GLES20.glUniform2f(mTexDimHandleEffect, width, height);
		GLES20.glUniform2f(mTexDimHandleEffect, 1.0f, 1.0f);
		GLES20.glUniform2fv(mScaleHandleEffect, 1, scale, 0);
		GLES20.glUniform2fv(mOffsetHandleEffect, 1, offset, 0);
		GLES20.glUniform2fv(mOriginHandleEffect, 1, origin, 0);
		GLES20.glUniform1f(mDivisionsHandleEffect, divisions); 
		GLES20.glUniform1f(mDivAmountHandleEffect, divAmount); 
		    
	}
//------------------------------------------------------------------------------------
		public void drawKaleidoscopeShader(int fboTextureIdx, int vertexBufferIdx, float beatAveraged)
		{ 
			averagedBeatSamples = beatAveraged;
			//mQuadTextureBufferIdx = fboTextureIdx;
			//----------------------------------------------
		//	GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			//pass frame buffer to shader and draw screen
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, fboTextureIdx);
				
			useAndSendGLSL();
			
			buildEffectMatrices();
		    				 
		    GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vertexBufferIdx);
		    GLES20.glEnableVertexAttribArray(maPositionHandleEffect);
		    GLES20.glVertexAttribPointer(maPositionHandleEffect, 3, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, 0);
		  
		    GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vertexBufferIdx);
		    GLES20.glEnableVertexAttribArray(mTextureCoordinateHandleEffect);
		    GLES20.glVertexAttribPointer(mTextureCoordinateHandleEffect, 2, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, TRIANGLE_VERTICES_DATA_TEX_OFFSET* FLOAT_SIZE_BYTES);
		    	
		    GLES20.glDrawArrays(GLES20.GL_TRIANGLES , 0,36);

			checkGlError("glDrawArrays");
			
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
			GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		}
//------------------------------------------------------------------------------------
		private void buildEffectMatrices()
		{ 
			// Set the view matrix. This matrix can be said to represent the camera position.
			Matrix.setLookAtM(mViewMatrixEffect, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
			
			Matrix.frustumM(mProjectionMatrixEffect, 0, left, right, bottom, top, near, far);
			
			Matrix.setIdentityM(mModelMatrixEffect, 0);
			Matrix.translateM(mModelMatrixEffect, 0, 0.0f, 0.0f, -4.5f);
			
			Matrix.rotateM(mModelMatrixEffect, 0, 270.0f, 0.0f, 0.0f, 1.0f);
		   // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
		   // (which currently contains model * view).
		   Matrix.multiplyMM(mMVPMatrixEffect, 0, mViewMatrixEffect, 0, mModelMatrixEffect, 0);   
			   
		   // Pass in the model view matrix.
		   GLES20.glUniformMatrix4fv(mMVMatrixHandleEffect, 1, false, mMVPMatrixEffect, 0);                
			   
		   // This multiplies the model view matrix by the projection matrix, and stores the result in the MVP matrix
		   // (which now contains model * view * projection).        
		   Matrix.multiplyMM(mTemporaryMatrixEffect, 0, mProjectionMatrixEffect, 0, mMVPMatrixEffect, 0);
		   System.arraycopy(mTemporaryMatrixEffect, 0, mMVPMatrixEffect, 0, 16);
			
		   // Pass in the combined matrix.
		   GLES20.glUniformMatrix4fv(mMVPMatrixHandleEffect, 1, false, mMVPMatrixEffect, 0);
		   //checkGlError("buildFrameBufferMatrices");	
		}			
//------------------------------------------------------------------------------------
}
