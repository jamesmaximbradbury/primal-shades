package audio.graphics.shaders;





import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import audio.graphics.shaders.utils.Object3D;

import audio.graphics.shaders.utils.RecorderThread;

import audio.graphics.shaders.OscPlugin;

import android.content.Context;
import android.graphics.Point;
//import android.graphics.Bitmap;
//import android.graphics.Bitmap.Config;
//import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
//import android.opengl.GLUtils;
import android.opengl.Matrix;

import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class AGS_GLES20Renderer implements GLSurfaceView.Renderer 
{
//----------------------------------------------------------------------------------------   
private static final String TAG = "AGS_GLES20Renderer";

private Context mContext;
public static int screenWidth;
public static int screenHeight;
// The objects
Object3D[] objectStructures = new Object3D[3];
// Store the model matrix. This matrix is used to move models from object space (where each model can be thought
// of being located at the center of the universe) to world space.
private float[] mModelMatrix = new float[16];
// Store the view matrix. This can be thought of as our camera. This matrix transforms world space to eye space;
// it positions things relative to our eye.
private float[] mViewMatrix = new float[16];
// Store the projection matrix. This is used to project the scene onto a 2D viewport. 
private float[] mProjectionMatrix = new float[16];
private float[] mScaleMatrix = new float[16];   // scaling
private float[] mRotXMatrix = new float[16];	// rotation x
private float[] mRotYMatrix = new float[16];	// rotation x

public ShortBuffer recordedSamples;
public FloatBuffer normalisedSamples;
public float averagedNormaliseBeat;
public ShortBuffer recordedSamplesHistory;
public FloatBuffer normalisedSamplesHistory;
public float averagedNormaliseBeatHistory;

private int runOnce = -1;

short[] indices;

public float mAngleX;
public float mAngleY;

// scaling 
float scaleX = 1.0f;
float scaleY = 1.0f;
float scaleZ = -10.0f;

//classes ----------------------------
private RecorderThread recorderThread;
//plugins
private OscPlugin oscilator;
private CubePlugin cubeIsm;
private BarAmpGraphPlugin barAmp;
private SpectroGraphPlugin spectro;

//--------------------------------
//--------------------------------
private boolean useOsc = true;
private boolean useCube= true;
private boolean useBars = false;
private boolean useSpectro = false;
//--------------------------------
private int numberFFTElements;
//--------------------------------

//----------------------------------------------------------------------------------------       
public AGS_GLES20Renderer(Context context) 
{
	mContext = context;		
	
	WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
	Display display = wm.getDefaultDisplay();
	Point size = new Point();
	display.getSize(size);
	screenWidth = size.x;
	screenHeight = size.y-42;
	    
	
    recorderThread = new RecorderThread();
   
	recorderThread.start();
	
	//-----------------------------------------
/*	String oglVersion = "";
	String deviceName = "";
	String extensions = "";
	
	 oglVersion = GLES20.glGetString(GLES20.GL_VERSION);
	 deviceName = GLES20.glGetString(GLES20.GL_RENDERER);
	 extensions = GLES20.glGetString(GLES20.GL_EXTENSIONS);
	if(oglVersion!=null)
	{
		 Log.w(oglVersion," version");
	}*/
	/*  const char* version = (const char*) glGetString(GLES20.GL_VERSION);
	    mVersion = strdup(version);

	    // Section 6.1.5 of the OpenGL ES specification indicates the GL version
	    // string strictly follows this format:
	    //
	    // OpenGL<space>ES<space><version number><space><vendor-specific information>
	    //
	    // In addition section 6.1.5 describes the version number thusly:
	    //
	    // "The version number is either of the form major number.minor number or
	    // major number.minor number.release number, where the numbers all have one
	    // or more digits. The release number and vendor specific information are
	    // optional."

	    if (sscanf(version, "OpenGL ES %d.%d", &mVersionMajor, &mVersionMinor) !=2) {
	        // If we cannot parse the version number, assume OpenGL ES 2.0
	        mVersionMajor = 2;
	        mVersionMinor = 0;
	    }*/

}
//----------------------------------------------------------------------------------------    
public void onSurfaceCreated(GL10 unused, EGLConfig config) 
{	
    // Set the background frame colour
    GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
 
    // Use culling to remove back faces.
 	GLES20.glEnable(GLES20.GL_CULL_FACE);
	GLES20.glEnable(GLES20.GL_BLEND);
	//GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE);
	GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
 	// Enable depth testing
 	//GLES20.glEnable(GLES20.GL_DEPTH_TEST);

 	//construct plugins and load vertices into vbo's
	oscilator = new OscPlugin(mContext);
	
	cubeIsm = new CubePlugin(mContext);
	
	cubeIsm.addFloatVertices();
    
	barAmp = new BarAmpGraphPlugin(mContext);
	
	barAmp.addFloatVertices();
	
	spectro = new SpectroGraphPlugin(mContext);
	
	spectro.addFloatVertices();
  
    //Log.w(TAG,"init");	  	  
}	
//-----------------------------------------------------------------------------------------    
public void onSurfaceChanged(GL10 unused, int width, int height) 
{
    GLES20.glViewport(0, 0, width, height);
    
    // Create a new perspective projection matrix. The height will stay the same
 	// while the width will vary as per aspect ratio.
	final float ratio = (float) width / height;
//	final float left = -ratio;
//	final float right = ratio;
	final float left = -1.0f;
	final float right = 1.0f;
	final float bottom = -1.0f;
	final float top = 1.0f;
	final float near = 1.0f;
	final float far = 200.0f;
	
	Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
}
 
//---------------------------------------------------------
public void onDrawFrame(GL10 unused) 
{
//	recorderThread.stopRecording();
	
	//if(!recorderThread.isRecording())
		//recorderThread.startRecording();
	
//	recordedSamples =  recorderThread.getFrameShortBuffer();
//	normalisedSamples = recorderThread.getNormalisedFloatBuffer(recordedSamples);
//	if(normalisedSamples!=null)
//		averagedNormaliseBeat = recorderThread.getNormalisedAverage(normalisedSamples);
//	else
	//if(averagedNormaliseBeat>=0.999)
	//{
/*		do{
		recordedSamples =  recorderThread.getFrameShortBuffer();
		normalisedSamples = recorderThread.getNormalisedFloatBuffer(recordedSamples);
		}while (normalisedSamples == null);
		
		averagedNormaliseBeat = recorderThread.getNormalisedAverage(normalisedSamples);
	*/	
	//}
		/*
			recordedSamples =  recorderThread.getFrameShortBuffer();
			normalisedSamples = recorderThread.getNormalisedFloatBuffer(recordedSamples);
			if (normalisedSamples != null)
			{
				averagedNormaliseBeat = recorderThread.getNormalisedAverage(normalisedSamples);
				recordedSamplesHistory = recordedSamples;
				normalisedSamplesHistory = normalisedSamples;
				averagedNormaliseBeatHistory = averagedNormaliseBeat;
				
			}
			else
			{
				recordedSamples = recordedSamplesHistory;
				normalisedSamples = normalisedSamplesHistory;
				averagedNormaliseBeat= averagedNormaliseBeatHistory;			
			}
		*/
	recordedSamples =  recorderThread.getFrameShortBuffer();
	normalisedSamples = recorderThread.getNormalisedFloatBuffer(recordedSamples);
	averagedNormaliseBeat = recorderThread.getNormalisedAverage(normalisedSamples);
	
	
	if(averagedNormaliseBeat>=0.999)
	{
		recordedSamples = recordedSamplesHistory;
		normalisedSamples = normalisedSamplesHistory;
		averagedNormaliseBeat= averagedNormaliseBeatHistory;
	}
	else
	{
		recordedSamplesHistory = recordedSamples;
		normalisedSamplesHistory = normalisedSamples;
		averagedNormaliseBeatHistory = averagedNormaliseBeat;
	}
	
	  // Redraw background colour
	GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);// | GLES20.GL_DEPTH_BUFFER_BIT);
	
//	GLES20.glEnable(GLES20.GL_BLEND);
//	GLES20.glBlendFunc(1, 1);
//	GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
	//GLES20.glBlendFunc(GLES20.GL_ONE_MINUS_SRC_ALPHA, GLES20.GL_SRC_ALPHA);
	
	if(useSpectro)
		drawSpectro();
	
//	recorderThread.stopRecording();
	
	if(useOsc) //draw oscilloscope - calc vertices	
		drawOsc();
	
	if(useCube)//already loaded vertices - just calc matrices and drawarrays
		drawCube();
	
	if(useBars)
		drawBarAmp();
	
	//recorderThread.stopRecording();
	
	
//	recorderThread.startRecording();
	
}
//-----------------------------------------------------------------------------------------
private void drawSpectro()
{
	
	int numBlocks = 32; 

	numberFFTElements = numBlocks*2; //the number of frequency blocks is equal to /2 -1
	int numFreqBlocks = numBlocks*2-1;
	float[] mag = new float[numBlocks*2];
	
	mag = recorderThread.getFFT(recordedSamples, numberFFTElements);
	
	spectro.drawArray(mag, numBlocks, averagedNormaliseBeat);
	
}
//-----------------------------------------------------------------------------------------
private void drawBarAmp()
{
	
	//barAmp.useAndSendGLSL(recorderThread.getNormalisedAverage(recorderThread.getNormalisedFloatBuffer( recorderThread.getFrameShortBuffer())));
	
	barAmp.drawArray(averagedNormaliseBeat);
	/*
	int numBars = 6;
	float posBars = -7.0f;
	float posYBars = -1.0f;
	float offset = 2.0f;
	float rotation =0.0f;
	
	//  GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, barAmp.frameBufferID);	
	//  GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	//  barAmp.useAndSendGLSL(averagedNormaliseBeat);
	for (int x =0;x<numBars; x++)
	{
		posBars = posBars + offset ;
		//barAmp.buildMatrices(rotation,posBars,posYBars);
		barAmp.drawArray(rotation,posBars,posYBars,averagedNormaliseBeat);	
	}
	
	rotation =180.0f;
	posYBars = 1.0f;
	posBars = -7.0f;
	for (int x =0;x<numBars; x++)
	{
		posBars = posBars + offset ;
		//barAmp.buildMatrices(rotation,posBars,posYBars);
		barAmp.drawArray(rotation,posBars,posYBars,averagedNormaliseBeat);	
	}
	*/
	//GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
}
//-----------------------------------------------------------------------------------------
private void drawOsc()
{
	oscilator.addFloatVertices(normalisedSamples);
	
	//oscilator.useAndSendGLSL();
	
//	oscilator.buildMatrices();
	
	oscilator.drawArray(averagedNormaliseBeat);
}
//-----------------------------------------------------------------------------------------
private void drawCube()
{
	cubeIsm.useAndSendGLSL(averagedNormaliseBeat);
	
	cubeIsm.buildMatrices(scaleZ,mAngleX,mAngleY);
	
	cubeIsm.drawArray();
}
//------------------------------------------------------------------------   
// debugging opengl
private void checkGlError(String op) 
{
	int error;
	while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
		Log.e(TAG, op + ": glError " + error);
		throw new RuntimeException(op + ": glError " + error);
	}
}
//------------------------------------------------------------------------
public void changeScale(float scale) 
{
	scaleZ = -scale /(20);
	if(scaleZ>-5.0)
		scaleZ=-5.0f;
	
/*	if (scaleX * scale > 1.4f)
		return;
	scaleX *= scale;
	scaleY *= scale;
	scaleZ *= scale;
*/
//	Log.d("SCALE: ", scaleZ + "");	
}
}