/*
 * Copyright (C) 2012 Jacquet Wong
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * musicg api in Google Code: http://code.google.com/p/musicg/
 * Android Application in Google Play: https://play.google.com/store/apps/details?id=com.whistleapp
 * 
 */

package audio.graphics.shaders.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.audiofx.Visualizer;
import android.util.Log;
import audio.graphics.shaders.utils.FloatFFT_1D;
import audio.graphics.shaders.utils.FFT;
import audio.graphics.shaders.AGS_GLES20Renderer;

public class RecorderThread extends Thread {
	
	private final String TAG = "Record Thread";
	private final int sampleFreq = 16000;
	private static final int SHORT_SIZE_BYTES = 2;
	private static final int FLOAT_SIZE_BYTES = 4;
	private AudioRecord audioRecord;
	//private AudioRecord audioRecorder;
	private boolean isRecording;
	private int channelConfiguration = AudioFormat.CHANNEL_IN_MONO;//  CHANNEL_IN_MONO CHANNEL_CONFIGURATION_MONO;// CHANNEL_CONFIGURATION_DEFAULT CHANNEL_CONFIGURATION_MONO
	private int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
	//private int sampleRate = 44100;
	private int sampleRate = sampleFreq;
	public final static int frameByteSize =AGS_GLES20Renderer.screenWidth *2  ; //=1600 for this //6400;//3200;//4096;//2048; // for 1024 fft size (16bit sample size)
	private static final int sampleSize = frameByteSize/2;
	private static final int averagedSampleSize = sampleSize/2;
	byte[] buffer;
	private byte[] mBytes;
	private FloatBuffer vertexBuffer;
	
	private FloatFFT_1D FFTData;
	//private FFT FFTIData;
	// private MediaPlayer mPlayer;
	private Visualizer mVisualizer;
	
	public RecorderThread()
	{
	//	MediaPlayer mPlayer = new MediaPlayer();
		//mPlayer = MediaPlayer.create(this);
	    //mPlayer.setLooping(true);
	   // mPlayer.start();
     
		int recBufSize = AudioRecord.getMinBufferSize(sampleRate, channelConfiguration, audioEncoding); // need to be larger than size of a frame
	//	
		//Log.i("buffer size ", String.valueOf(recBufSize) );
		//	audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,  44100,  AudioFormat.CHANNEL_IN_DEFAULT,  AudioFormat.ENCODING_PCM_16BIT,  recBufSize);
			
		audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleRate, audioEncoding, audioEncoding, recBufSize*12); //*12
		//int status = audioRecord.getState();
        //if (status == AudioRecord.STATE_INITIALIZED)
        //	Log.i("status ", String.valueOf(status) );
		buffer = new byte[frameByteSize]; //frameByteSize
	
	}
//-------------------------------------------------------------------------------------------
	public void updateVisualizer(byte[] bytes) {
        mBytes = bytes;
        //invalidate();
    }
//-------------------------------------------------------------------------------------------	
	private void setupVisualizerFxAndUI() 
	{
		mVisualizer = new Visualizer(0);
		mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		 	
        mVisualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() 
        {	 	
	        public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) 
	        {
	               updateVisualizer(bytes);
	        }
	 	
	        public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) 
	        {
	        	
	        }
	        
        }, Visualizer.getMaxCaptureRate() / 2, true, false);
        
	}
//-------------------------------------------------------------------------------------------	
	public AudioRecord getAudioRecord(){
		return audioRecord;
	}
//-------------------------------------------------------------------------------------------
	public boolean isRecording(){
		return this.isAlive() && isRecording;
	}
//-------------------------------------------------------------------------------------------
	public void startRecording(){
		try{
			audioRecord.startRecording();
			isRecording = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//-------------------------------------------------------------------------------------------	
	public void stopRecording(){
		try{
			audioRecord.stop();
			isRecording = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//-------------------------------------------------------------------------------------------
public float[] getFFT(ShortBuffer vertices, int numberElements)
{
	
	int fftSize =  numberElements; //numberElements*2;
	float[] a =  new float[fftSize];  // the array of the fft data to be sent and returned in this array
	double[] aD =  new double[fftSize];
	double[] aI =  new double[fftSize];
	byte[] fftBuffer = new byte[fftSize*2];  //the read data from the audio stream that is copied into a[]
	float[] mag = new float[numberElements]; // the magnitudes of the returned fft data 
	ShortBuffer shortVertices;
	ByteBuffer byteVertices;
//	float[] a =  new float[sampleSize];
//	float averageSamples =0.0f;
//	int numFreqBlocks = numberElements-1;
	//if(!isRecording())
	//	startRecording();
	//-------------------------------
	//read the audio data in
	//The size of the FFT will be the size of your audioBuffer / 2 
	int bytesRead = audioRecord.read(fftBuffer, 0, numberElements*2); //these are bytes so need 2 for each 16bit sample
	//Log.i("buffer length ", String.valueOf(buffer.length) );
	if(bytesRead==0)
	{
		Log.e(TAG, "NO bytes were read" );
		return null;
	}
	//else
	//	Log.i("Audio Bytes Read : ", String.valueOf(bytesRead));
	
	//-------------------------------
	//convert the 16bit byte data into short int's 
	byteVertices = ByteBuffer.allocateDirect(bytesRead).order(ByteOrder.nativeOrder()); //* FLOAT_SIZE_BYTES
	byteVertices.put(fftBuffer);
	byteVertices.position(0);
	
	shortVertices= byteVertices.asShortBuffer();
	
	//-------------------------------
	//init construct of fft class with size of audio buffer *2
	FFTData = new FloatFFT_1D(fftSize);
	//FFTIData = new FFT(fftSize);
	//copy read audio buffer into front half of array that will be passed to fft class
//	int count =0;
/*	for(int y=0; y<numberElements; y++)
	{
		aD[y] =  shortVertices.get(y) / 32768.0;
		aI[y] = 0.0;
	}
*/
	for(int y=0; y<numberElements; y++)
	{
		a[y] =  shortVertices.get(y) / 32768.0f;
	//	a[y] =   shortVertices.get(y);
	}
	//-------------------------------
/*
	for(int x=0; x<numberElements;x++) 
	{
	Log.i("audio data in a[] ", String.valueOf( x )+ " : " +String.valueOf(a[x] ));
	}
*/
	//-------------------------------
	//send the a[] data to do the fft calculation
//	FFTIData.fft(aD, aI);
	
	FFTData.realForward(a);
	//FFTData.realForwardFull(a);
//	FFTData.realForward(a, 0);
	// a[] should now have 2 values (real & imaginary) in this per 1 audio samples sent in
/*
	for(int x=0; x<fftSize;x++) 
	{
	Log.i("fft ", String.valueOf( x )+ " : " +String.valueOf(a[x] ));
	}
*/	 
	//-------------------------------
	// return the magnitudes of a[] and will take the fftSize and return it 
	//to the original size of numberElements
	mag = getMagnitudes(a, fftSize);
//	mag = getMagnitudes(aD,aI, fftSize);
/*
	for(int x=0; x<numberElements;x++)
	{
	Log.i("mag ", String.valueOf( x )+ " : " +String.valueOf(mag[x] ));
	}
*/	
	return mag;
}
//-------------------------------------------------------------------------------------------
public float[] getMagnitudes(float[] a, int numFreqBlocks)
{
	float[] mag = new float[numFreqBlocks];
	int magElement = 0;
	float dbValue = 0;
	float temp = 0.0f;
	
	for(int x=2; x<numFreqBlocks;x+=2)
	{
		//Log.i("magElement ", String.valueOf(magElement ));
		//dbValue  = (a[x]*a[x]+a[x+1]*a[x+1]);
		temp  = (float)Math.sqrt(a[x]*a[x]+a[x+1]*a[x+1]);
	//	dbValue = (float) (10 * Math.log10(temp));
	//	mag[magElement] = 1.0f / Math.abs(dbValue);
	//	mag[magElement] = dbValue/250.0f;
//		dbValue  = (float)Math.sqrt(a[x]*a[x]+a[x+1]*a[x+1]);			
		//mag[magElement] = (float) (10 * Math.log10(dbValue));
		//mag[magElement] = dbValue;
		mag[magElement] = temp;
		magElement++;
	}
	return mag;
}
/*
public float[] getMagnitudes(double[] a,double[] i, int numFreqBlocks)
{
	float[] mag = new float[numFreqBlocks];
	int magElement = 0;
	float dbValue = 0;
	float temp = 0.0f;
	
	for(int x=2; x<numFreqBlocks;x++)
	{
		//Log.i("magElement ", String.valueOf(magElement ));
		//dbValue  = (a[x]*a[x]+a[x+1]*a[x+1]);
		temp  = (float)Math.sqrt(a[x]*a[x]+i[x]*i[x]);
	//	dbValue = (float) (10 * Math.log10(temp));
	//	mag[magElement] = 1.0f / Math.abs(dbValue);
	//	mag[magElement] = dbValue/250.0f;
//		dbValue  = (float)Math.sqrt(a[x]*a[x]+a[x+1]*a[x+1]);			
		//mag[magElement] = (float) (10 * Math.log10(dbValue));
		//mag[magElement] = dbValue;
		mag[magElement] = temp;
		magElement++;
	}
	return mag;
}
*/
//-------------------------------------------------------------------------------------------
	public float getNormalisedAverage(FloatBuffer vertices)
	{
		float totalNormals = 0.0f;
		float averagedNormals = 0.0f;
		//float ampNormalised = 1.0f;
		
		for(int x=0; x<sampleSize;x++)//for(int x=0; x<averagedSampleSize/2;x++)
		{
			totalNormals = totalNormals + Math.abs(vertices.get(x));
		}
		vertices.position(0);
		
		//averagedNormals = (totalNormals / (averagedSampleSize/2));//*ampNormalised;
		averagedNormals = (totalNormals / sampleSize);
		
		if(averagedNormals>=0.999)
		{
			stopRecording();
			startRecording();
		}
		
	//	stopRecording();
	//	startRecording();
		
		//Log.i("averaged normalised ", String.valueOf(averagedNormals) );
	//	Log.i("float buffer ", String.valueOf(vertices.get(0)) );
	//	Log.i("total normalised ", String.valueOf(totalNormals) );
		return averagedNormals;	
	}
//-------------------------------------------------------------------------------------------
	public FloatBuffer getNormalisedFloatBuffer(ShortBuffer vertices)
	{
		float counter=0.0f;
		float averageSamples =0.0f;
		float sampleNum = -400.0f;
		vertexBuffer = ByteBuffer.allocateDirect((sampleSize)*FLOAT_SIZE_BYTES).order(ByteOrder.nativeOrder()).asFloatBuffer();
		//vertexBuffer = ByteBuffer.allocateDirect((sampleSize/4)*FLOAT_SIZE_BYTES).order(ByteOrder.nativeOrder()).asFloatBuffer();
		vertexBuffer.position(0);
	//	vertexBuffer.put (vertices. );
		for(int x=0; x<sampleSize;x++)
		{
			if(x<sampleSize-1)
				averageSamples = (vertices.get(x)+vertices.get(x+1))/2;
			else
				averageSamples = vertices.get(x);
			
			averageSamples = averageSamples/32768.0f;
			vertexBuffer.put (averageSamples );
			
		/*	if(averageSamples<=-0.999)
			{
				stopRecording();
				startRecording();
				return null;
			}
			*/
		}
	/*	for(int x=0; x<sampleSize;x+=4)
		{
			averageSamples = (vertices.get(x)+vertices.get(x+1)+vertices.get(x+2)+vertices.get(x+3))/4;
			averageSamples = averageSamples/32768.0f;
			vertexBuffer.put (averageSamples );
		}
	*/
		/*
		for(int x=0; x<sampleSize;x+=2)
		{
			averageSamples = (vertices.get(x)+vertices.get(x+1))/2;
			averageSamples = averageSamples/32768;
			//counter = sampleNum / 400.0f;
			vertexBuffer.put (averageSamples );
			//vertexBuffer.put(averageSamples*scaleAmpVert);
			//vertexBuffer.put(0.0f);
			//sampleNum++;
		}
		*/
	//	Log.i("short buffer vertices ", String.valueOf(vertices.get(0)) );
	//	Log.i("averaged samples ", String.valueOf(averageSamples) );
	//	Log.i("vertex buffer normals ", String.valueOf(vertexBuffer.get(0)) );
		
		vertexBuffer.position(0);
		
	return vertexBuffer;	
	}
//-------------------------------------------------------------------------------------------
	public ShortBuffer getFrameShortBuffer()
	{
		//int floatArraySize = frameByteSize/2;
		//float [] floatArray = new float[floatArraySize];
		ByteBuffer byteVertices;
		ShortBuffer shortVertices;
		
		int bytesRead = audioRecord.read(buffer, 0, frameByteSize); //1600
		//Log.i("buffer length ", String.valueOf(buffer.length) );
		if(bytesRead<=0)
		{
			Log.e(TAG, "NO bytes were read" );
			return null;
		}
		//else
		//	Log.i("Audio Bytes Read : ", String.valueOf(bytesRead));
		
		byteVertices = ByteBuffer.allocateDirect(buffer.length).order(ByteOrder.nativeOrder()); //* FLOAT_SIZE_BYTES
		byteVertices.put(buffer);
		byteVertices.position(0);
		
		shortVertices= byteVertices.asShortBuffer();
		
		
		
	//	shortVertices.put(byteVertices.asShortBuffer());
	//	shortVertices.position(0);
		

		return shortVertices;	
	}
//-------------------------------------------------------------------------------------------
	public byte[] getFrameBytes(){
		audioRecord.read(buffer, 0, frameByteSize);
		
		// analyze sound
	/*	int totalAbsValue = 0;
        short sample = 0; 
        float averageAbsValue = 0.0f;
        
        for (int i = 0; i < frameByteSize; i += 2) {
            sample = (short)((buffer[i]) | buffer[i + 1] << 8);
            totalAbsValue += Math.abs(sample);
        }
        averageAbsValue = totalAbsValue / frameByteSize / 2;
        
       // Log.i("frame samples ", String.valueOf(averageAbsValue) );
        //System.out.println(averageAbsValue);
        
        // no input
        if (averageAbsValue < 30){
        	return null;
        }
      */  
		return buffer;
	}
//-------------------------------------------------------------------------------------------	
	@Override
	public void run() {
		startRecording();
	}

	public void release() {
		audioRecord.release();
	}
}