//----------------------------------------------------------------------
// Project:
// By James Bradbury of Primal Visions
//----------------------------------------------------------------------
package audio.graphics.shaders;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import audio.graphics.shaders.utils.RawResourceReader;
import audio.graphics.shaders.utils.ShaderHelper;

//----------------------------------------------------------------------
// @author maxamillion
// 
//----------------------------------------------------------------------
public class WaveEffect extends EffectBase
{
	private final String TAG = "Wave Effect";
	int mVecSkillHandleEffect;
	int mVecTimeHandleEffect;
	float averagedBeatSamples;
	//----------------------------------------------------------------------
	// @param context
	//----------------------------------------------------------------------
	public WaveEffect(Context context)
	{
		super(context);
		
		final String vertexShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.pass_thru_vs);   		
	 	final String fragmentShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.waves_fs);			
			
		final int vertexShaderHandle = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);		
		final int fragmentShaderHandle = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);		
			
		mProgramEffect = ShaderHelper.createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle,new String[] {"a_Position","a_TexCoordinate"}); 
			
		// get handle to the vertex shader's member
	    maPositionHandleEffect = GLES20.glGetAttribLocation(mProgramEffect, "a_Position");
	    mTextureCoordinateHandleEffect = GLES20.glGetAttribLocation(mProgramEffect, "a_TexCoordinate");
	    mMVPMatrixHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_MVPMatrix");
	    mMVMatrixHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_MVMatrix"); 
	    // get handle to fragment shader's  member
	    //mColorHandle = GLES20.glGetUniformLocation(mProgramEffect, "vColor");
	    mTextureUniformHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "u_Texture");
	    mVecSkillHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "vecSkill");
	    mVecTimeHandleEffect = GLES20.glGetUniformLocation(mProgramEffect, "vecTime");
	
	    checkGlError("wave constructor");	
	}
	//------------------------------------------------------------------------------------	
		private void useAndSendGLSL()
		{
			float[] VecSkill = {  30.0f, averagedBeatSamples}; 
			//float[] VecSkill = {  30.0f, 0.1f}; 
			float time =  2.0f*averagedBeatSamples; 
			//float time =  2.0f; 
		
			GLES20.glUseProgram(mProgramEffect);
			
			GLES20.glUniform1i(mTextureUniformHandleEffect, 0);
			 
			//GLES20.glUniform2f(mTexDimHandleEffect, width, height);
			GLES20.glUniform2fv(mVecSkillHandleEffect, 1, VecSkill, 0);
			GLES20.glUniform1f(mVecTimeHandleEffect, time); 

			    
		}
	//------------------------------------------------------------------------------------
			public void drawWaveShader(int fboTextureIdx, int vertexBufferIdx, float beatAveraged)
			{ 
				averagedBeatSamples = beatAveraged;
				//mQuadTextureBufferIdx = fboTextureIdx;
				//----------------------------------------------
			//	GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
				//pass frame buffer to shader and draw screen
				GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, fboTextureIdx);
					
				useAndSendGLSL();
				
				buildEffectMatrices();
			    				 
			    GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vertexBufferIdx);
			    GLES20.glEnableVertexAttribArray(maPositionHandleEffect);
			    GLES20.glVertexAttribPointer(maPositionHandleEffect, 3, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, 0);
			  
			    GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vertexBufferIdx);
			    GLES20.glEnableVertexAttribArray(mTextureCoordinateHandleEffect);
			    GLES20.glVertexAttribPointer(mTextureCoordinateHandleEffect, 2, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, TRIANGLE_VERTICES_DATA_TEX_OFFSET* FLOAT_SIZE_BYTES);
			    	
			    GLES20.glDrawArrays(GLES20.GL_TRIANGLES , 0,36);

				checkGlError("glDrawArrays");
				
				GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
				GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
			}
	//------------------------------------------------------------------------------------
			private void buildEffectMatrices()
			{ 
				// Set the view matrix. This matrix can be said to represent the camera position.
				Matrix.setLookAtM(mViewMatrixEffect, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
				
				Matrix.frustumM(mProjectionMatrixEffect, 0, left, right, bottom, top, near, far);
				
				Matrix.setIdentityM(mModelMatrixEffect, 0);
				Matrix.translateM(mModelMatrixEffect, 0, 0.0f, 0.0f, -4.5f);
				
				Matrix.rotateM(mModelMatrixEffect, 0, 270.0f, 0.0f, 0.0f, 1.0f);
			   // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
			   // (which currently contains model * view).
			   Matrix.multiplyMM(mMVPMatrixEffect, 0, mViewMatrixEffect, 0, mModelMatrixEffect, 0);   
				   
			   // Pass in the model view matrix.
			   GLES20.glUniformMatrix4fv(mMVMatrixHandleEffect, 1, false, mMVPMatrixEffect, 0);                
				   
			   // This multiplies the model view matrix by the projection matrix, and stores the result in the MVP matrix
			   // (which now contains model * view * projection).        
			   Matrix.multiplyMM(mTemporaryMatrixEffect, 0, mProjectionMatrixEffect, 0, mMVPMatrixEffect, 0);
			   System.arraycopy(mTemporaryMatrixEffect, 0, mMVPMatrixEffect, 0, 16);
				
			   // Pass in the combined matrix.
			   GLES20.glUniformMatrix4fv(mMVPMatrixHandleEffect, 1, false, mMVPMatrixEffect, 0);
			   //checkGlError("buildFrameBufferMatrices");	
			}			
	//------------------------------------------------------------------------------------
}
