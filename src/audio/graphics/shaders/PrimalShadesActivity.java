package audio.graphics.shaders;



import audio.graphics.shaders.utils.RecorderThread;


import android.app.Activity;
//import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.FloatMath;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

//------------------------------------------------------------------------------------------
public class PrimalShadesActivity extends Activity {
    /** Called when the activity is first created. */
	private static final String TAG = "Actovity";
	private GLSurfaceView mGLSurfaceView;
	 AGS_GLES20Renderer renderer;
	// detection parameters
	//	private DetectorThread detectorThread;
	//	private RecorderThread recorderThread;
	// rotation
		private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
		private float mPreviousX;
		private float mPreviousY;

		// touch events
		private final int NONE = 0;
		private final int DRAG = 0;
		private final int ZOOM = 0;

		// pinch to zoom
		float oldDist = 100.0f;
		float newDist;

		int mode = 0;
		
	
	 //
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    	// Create a new GLSurfaceView - this holds the GL Renderer
		mGLSurfaceView = new GLSurfaceView(this);
		mGLSurfaceView.setEGLContextClientVersion(2);
		
		renderer = new AGS_GLES20Renderer(this);
		mGLSurfaceView.setRenderer(renderer);
				 
        setContentView(mGLSurfaceView);
    	
        //recorderThread = new RecorderThread();
      
    	//recorderThread.start();
   
    	//recorderThread.run();
    	
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        // The following call pauses the rendering thread.
        // If your OpenGL application is memory intensive,
        // you should consider de-allocating objects that
        // consume significant memory here.
        mGLSurfaceView.onPause();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        // The following call resumes a paused rendering thread.
        // If you de-allocated graphic objects for onPause()
        // this is a good place to re-allocate them.
        mGLSurfaceView.onResume();
    }

    @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			//goHomeView();
			android.os.Process.killProcess(android.os.Process.myPid());
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
    
    protected void onDestroy() {
		super.onDestroy();
		//recorderThread.release();
		android.os.Process.killProcess(android.os.Process.myPid());
	}
    
/************
 * TOUCH FUNCTION - Should allow user to rotate the environment
 **********/
@Override public boolean onTouchEvent(MotionEvent e) {
	float x = e.getX();
	float y = e.getY();
	switch (e.getAction()) {
	case MotionEvent.ACTION_DOWN:			// one touch: drag
		Log.d("ShaderActivity", "mode=DRAG" );
		mode = DRAG;
		break;
	case MotionEvent.ACTION_POINTER_DOWN:	// two touches: zoom
		Log.d("ShaderActivity", "mode=ZOOM" );
		oldDist = spacing(e);
		if (oldDist > 100.0f) {
			mode = ZOOM; // zoom
		}
		break;
	case MotionEvent.ACTION_UP:		// no mode
		mode = NONE;
		Log.d("ShaderActivity", "mode=NONE" );
		oldDist = 100.0f;
		break;
	case MotionEvent.ACTION_POINTER_UP:		// no mode
		mode = NONE;
		Log.d("ShaderActivity", "mode=NONE" );
		oldDist = 10.0f;
		break;
	case MotionEvent.ACTION_MOVE:						// rotation
		if (e.getPointerCount() > 1 && mode == ZOOM) {
			newDist = spacing(e);
			Log.d("SPACING: ", "OldDist: " + oldDist + ", NewDist: " + newDist);
			if (newDist > 10.0f) {
				float scale = newDist;///oldDist; // scale
				//float scale = newDist-oldDist;///oldDist; // scale
				// scale in the renderer
				renderer.changeScale(scale);

				oldDist = newDist;
			}
		}
		else if (mode == DRAG){
			
			float dx = x - mPreviousX;
			float dy = y - mPreviousY;
			//Log.d("X: ", String.valueOf(dx));
			renderer.mAngleX += dx * TOUCH_SCALE_FACTOR;
			renderer.mAngleY += dy * TOUCH_SCALE_FACTOR;
			//Log.d("mAngleX", String.valueOf(renderer.mAngleX ));
			mGLSurfaceView.requestRender();
		}
		break;
	}
	mPreviousX = x;
	mPreviousY = y;
	return true;
}

// finds spacing
private float spacing(MotionEvent event) {
	float x = event.getX(0) - event.getX(1);
	float y = event.getY(0) - event.getY(1);
	return FloatMath.sqrt(x * x + y * y);
}

}