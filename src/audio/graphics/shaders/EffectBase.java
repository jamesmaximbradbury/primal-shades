//----------------------------------------------------------------------
// Project:
// By James Bradbury of Primal Visions
//----------------------------------------------------------------------
package audio.graphics.shaders;

import android.content.Context;

//----------------------------------------------------------------------
// @author maxamillion
// 
//----------------------------------------------------------------------
public class EffectBase extends PluginBase
{
	protected  int mMVPMatrixHandleEffect;
	protected  int mMVMatrixHandleEffect;
	protected  int mProgramEffect;
	protected  int maPositionHandleEffect;
	protected  int mTextureUniformHandleEffect;
	protected  int mTextureCoordinateHandleEffect;
	
	protected  float[] mModelMatrixEffect = new float[16];
	protected  float[] mViewMatrixEffect = new float[16];
	protected  float[] mProjectionMatrixEffect = new float[16];
	protected  float[] mMVPMatrixEffect = new float[16];
	protected  float[] mTemporaryMatrixEffect = new float[16];
	//----------------------------------------------------------------------
	// @param context
	//----------------------------------------------------------------------
	public EffectBase(Context context)
	{
		super(context);
		//-----------------------
		// TODO
		//-----------------------
	}
	
}
