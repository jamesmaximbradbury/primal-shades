package audio.graphics.shaders;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.content.Context;
import android.graphics.Point;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import audio.graphics.shaders.utils.Mesh;
import audio.graphics.shaders.utils.Object3D;
import audio.graphics.shaders.utils.RawResourceReader;
import audio.graphics.shaders.utils.RecorderThread;
import audio.graphics.shaders.utils.ShaderHelper;

public class PluginBase 
{
//	public static byte[] vertices;
	protected final static int frameByteSize = RecorderThread.frameByteSize;// = 1600 6400;//3200;
	protected final static int sampleSize = frameByteSize/2;//1600;//2048;//16384;
	protected static int width =AGS_GLES20Renderer.screenWidth ;//= 800;
	protected static int height = AGS_GLES20Renderer.screenHeight ;// = 480;
	protected static final int SHORT_SIZE_BYTES = 2;
	protected static final int FLOAT_SIZE_BYTES = 4;
	
	protected static final int TRIANGLE_VERTICES_DATA_STRIDE_BYTES = 8 * FLOAT_SIZE_BYTES;
	protected static final int TRIANGLE_VERTICES_DATA_POS_OFFSET = 0;
	protected static final int TRIANGLE_VERTICES_DATA_NOR_OFFSET = 3;
	protected static final int TRIANGLE_VERTICES_DATA_TEX_OFFSET = 6;
	
	protected static final String TAG = "Plugin Base";
	
	final static float ratio = (float) width / height;
	final static float leftRatio = -ratio;
	final static float rightRatio = ratio;
	final static float left = -1.0f;
	final static float right = 1.0f;
	final static float bottom = -1.0f;
	final static float top = 1.0f;
	final static float near = 1.0f;
	final static float far = 200.0f;
	
	// Position the eye in front of the origin.
	final static float eyeX = 0.0f;
	final static float eyeY = 0.0f;
	static float eyeZ = -0.5f;

	// We are looking toward the distance
	final static float lookX = 0.0f;
	final static float lookY = 0.0f;
	final static float lookZ = -5.0f;

	// Set our up vector. This is where our head would be pointing were we holding the camera.
	final static float upX = 0.0f;
	final static float upY = 1.0f;
	final static float upZ = 0.0f;
	 
	//------------------------
	protected  Context mContext;
	protected  FloatBuffer vertexBuffer;
	protected  float[] mModelMatrix = new float[16];
	protected  float[] mViewMatrix = new float[16];
	/** Store the projection matrix. This is used to project the scene onto a 2D viewport. */
	protected  float[] mProjectionMatrix = new float[16];
	/** Allocate storage for the final combined matrix. This will be passed into the shader program. */
	protected  float[] mMVPMatrix = new float[16];
	
	protected  float[] mModelMatrixFrame = new float[16];
	protected  float[] mViewMatrixFrame = new float[16];
	/** Store the projection matrix. This is used to project the scene onto a 2D viewport. */
	protected  float[] mProjectionMatrixFrame = new float[16];
	/** Allocate storage for the final combined matrix. This will be passed into the shader program. */
	protected  float[] mMVPMatrixFrame = new float[16];
	
	protected  float[] mAccumulatedRotation = new float[16];
	protected  float[] mCurrentRotation = new float[16];
	/** A temporary matrix. */
	protected  float[] mTemporaryMatrix = new float[16];
	protected  float[] mTemporaryMatrixFrame = new float[16];
	
	protected  int mMVPMatrixHandlePassThru;
	protected  int mMVMatrixHandlePassThru;
	protected  int mProgramPassThru;
	protected  int maPositionHandlePassThru;
	protected  int mTextureUniformHandlePassThru;
	protected  int mTextureCoordinateHandlePassThru;
	
	protected int mQuadBufferIdx;
	protected int mQuadTextureBufferIdx;
	private FloatBuffer quadVB;
	
	private Object3D[] objectStructures = new Object3D[3];
	
	private int texture;

	

	
//-------------------------------------------------------------------------------
	public  PluginBase(Context context)
	{
		mContext = context;
		
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y-42;
		//Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
		
		//Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
		
		passThruShader();
		
		
	}
//-------------------------------------------------------------------------------
	protected int createTargetTexture() 
	{
		
      //  int texture;
        int[] textures = new int[1];
        GLES20.glGenTextures(1, textures, 0);
        texture = textures[0];
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, width, height, 0,  GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);//UNSIGNED_BYTE
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,  GLES20.GL_TEXTURE_MAG_FILTER,  GLES20.GL_LINEAR);
     //   GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,  GLES20.GL_CLAMP_TO_EDGE );//GL_REPEAT
    //    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,   GLES20.GL_CLAMP_TO_EDGE );//GL_REPEAT
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,  GLES20.GL_MIRRORED_REPEAT );//GL_REPEAT
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,   GLES20.GL_MIRRORED_REPEAT );//GL_REPEAT
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0); 
        
      //  mQuadTextureBufferIdx = texture;
        
        return texture;
    }
//------------------------------------------------------------------------
	protected int createFrameBuffer(int targetTextureId) 
	 {
         int framebuffer;
         int[] framebuffers = new int[1];
         GLES20.glGenFramebuffers(1, framebuffers, 0);
         framebuffer = framebuffers[0];
         GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, framebuffer);

         int depthbuffer;
         int[] renderbuffers = new int[1];
         GLES20.glGenRenderbuffers(1, renderbuffers, 0);
         depthbuffer = renderbuffers[0];

         GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, depthbuffer);
         GLES20.glRenderbufferStorage(GLES20.GL_RENDERBUFFER, GLES20.GL_DEPTH_COMPONENT16, width, height);
         GLES20.glFramebufferRenderbuffer(GLES20.GL_FRAMEBUFFER, GLES20.GL_DEPTH_ATTACHMENT,GLES20.GL_RENDERBUFFER, depthbuffer);
       
         GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER,GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, targetTextureId, 0);
         
         int status = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
         if (status != GLES20.GL_FRAMEBUFFER_COMPLETE) 
         {
        	 Log.w(TAG,"Frame buffer fail");
             throw new RuntimeException("Framebuffer is not complete: " +
                     Integer.toHexString(status));
         }
         //GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, 0);
         GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
         return framebuffer;
     }	
//------------------------------------------------------------------------   
	 protected FloatBuffer MakeBuffer(float[] arr)
	    {
	        ByteBuffer vbb = ByteBuffer.allocateDirect( 4 * 8);
	        vbb.order(ByteOrder.nativeOrder());
	        FloatBuffer fb = vbb.asFloatBuffer();
	        fb.put(arr);
	        fb.position(0);
	        return fb;
	    }
//------------------------------------------------------------------------   	
	// debugging opengl
	protected static void checkGlError(String op) 
	{
		int error;
		while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
			Log.e(TAG, op + ": glError " + error);
			throw new RuntimeException(op + ": glError " + error);
		}
	}
//------------------------------------------------------------------------  	
	protected int passThruShader()
	{
	final String vertexShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.pass_thru_vs);   		
 	final String fragmentShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.pass_thru_fs);			
		
	final int vertexShaderHandle = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);		
	final int fragmentShaderHandle = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);		
		
	mProgramPassThru = ShaderHelper.createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle,new String[] {"a_Position","a_TexCoordinate"}); 
		
	// get handle to the vertex shader's member
    maPositionHandlePassThru = GLES20.glGetAttribLocation(mProgramPassThru, "a_Position");
    mTextureCoordinateHandlePassThru = GLES20.glGetAttribLocation(mProgramPassThru, "a_TexCoordinate");
    mMVPMatrixHandlePassThru = GLES20.glGetUniformLocation(mProgramPassThru, "u_MVPMatrix");
    mMVMatrixHandlePassThru = GLES20.glGetUniformLocation(mProgramPassThru, "u_MVMatrix"); 
    // get handle to fragment shader's  member
    //mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
    mTextureUniformHandlePassThru = GLES20.glGetUniformLocation(mProgramPassThru, "u_Texture");
 
    final int buffers[] = new int[1];
	GLES20.glGenBuffers(1, buffers, 0);	
	
	try 
	{
		objectStructures[0] = new Object3D(R.raw.texturedcube, false, mContext);
	}   catch (Exception e)
	{
	//showAlert("" + e.getMessage());
		Log.w(TAG,"Failed to load mesh");
	};
	
	// Get buffers from mesh
	Mesh mesh = objectStructures[0].getMesh();
	quadVB = mesh.get_vb();

	GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
	GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER,quadVB.capacity()* FLOAT_SIZE_BYTES,quadVB, GLES20.GL_STATIC_DRAW); 
	GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

	mQuadBufferIdx = buffers[0];
	//mQuadTextureBufferIdx = texture; //textureBufferID
	GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
	
	quadVB.limit(0);
	quadVB = null;
	
	//checkGlError("mQuadBufferIdx");	
	return mQuadBufferIdx;	
	}	
//------------------------------------------------------------------------------------
	protected void drawPassThruShader(float[] translate , float[] rotateAngleAxisXYZ )
	{ 
		mQuadTextureBufferIdx = texture;
		//----------------------------------------------
	//	GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		//pass frame buffer to shader and draw screen
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mQuadTextureBufferIdx);
			
		GLES20.glUseProgram(mProgramPassThru);
		
		buildFrameBufferMatrices(translate,rotateAngleAxisXYZ);
		
	    GLES20.glUniform1i(mTextureUniformHandlePassThru, 0);
	    				 
	    GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mQuadBufferIdx);
	    GLES20.glEnableVertexAttribArray(maPositionHandlePassThru);
	    GLES20.glVertexAttribPointer(maPositionHandlePassThru, 3, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, 0);
	  
	    GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mQuadBufferIdx);
	    GLES20.glEnableVertexAttribArray(mTextureCoordinateHandlePassThru);
	    GLES20.glVertexAttribPointer(mTextureCoordinateHandlePassThru, 2, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, TRIANGLE_VERTICES_DATA_TEX_OFFSET* FLOAT_SIZE_BYTES);
	    	
	    GLES20.glDrawArrays(GLES20.GL_TRIANGLES , 0,36);

		checkGlError("glDrawArrays");
		
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
	}
//------------------------------------------------------------------------------------
		protected void buildFrameBufferMatrices(float[] translate , float[] rotateAngleAxisXYZ)
		{   
			// Set the view matrix. This matrix can be said to represent the camera position.
			Matrix.setLookAtM(mViewMatrixFrame, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
			
			Matrix.frustumM(mProjectionMatrixFrame, 0, left, right, bottom, top, near, far);
			
			Matrix.setIdentityM(mModelMatrixFrame, 0);
			Matrix.translateM(mModelMatrixFrame, 0, translate[0],translate[1],translate[2]);
			
			Matrix.rotateM(mModelMatrixFrame, 0, rotateAngleAxisXYZ[0], rotateAngleAxisXYZ[1], rotateAngleAxisXYZ[2],rotateAngleAxisXYZ[3]);
		   // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
		   // (which currently contains model * view).
		   Matrix.multiplyMM(mMVPMatrixFrame, 0, mViewMatrixFrame, 0, mModelMatrixFrame, 0);   
			   
		   // Pass in the model view matrix.
		   GLES20.glUniformMatrix4fv(mMVMatrixHandlePassThru, 1, false, mMVPMatrixFrame, 0);                
			   
		   // This multiplies the model view matrix by the projection matrix, and stores the result in the MVP matrix
		   // (which now contains model * view * projection).        
		   Matrix.multiplyMM(mTemporaryMatrixFrame, 0, mProjectionMatrixFrame, 0, mMVPMatrixFrame, 0);
		   System.arraycopy(mTemporaryMatrixFrame, 0, mMVPMatrixFrame, 0, 16);
			
		   // Pass in the combined matrix.
		   GLES20.glUniformMatrix4fv(mMVPMatrixHandlePassThru, 1, false, mMVPMatrixFrame, 0);
		   //checkGlError("buildFrameBufferMatrices");	
		}
				
	//------------------------------------------------------------------------------------
}
