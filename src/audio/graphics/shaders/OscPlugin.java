package audio.graphics.shaders;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import javax.microedition.khronos.opengles.GL11;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.util.Log;
import audio.graphics.shaders.utils.Mesh;
import audio.graphics.shaders.utils.Object3D;
import audio.graphics.shaders.utils.RawResourceReader;
import audio.graphics.shaders.utils.ShaderHelper;
import audio.graphics.shaders.utils.TextureHelper;

public class OscPlugin extends PluginBase {

	private static final String TAG = "Osc Plugin";
	//----------------------------	
	private  int mProgram;
	private  int maPositionHandle;
	private  int mAveragedSamplesHandle;
	private  int mColorHandle;
	private  int mColorEndHandle;
	private  int mMVPMatrixHandle;
	private  int mMVMatrixHandle;

	private  float mNormalisedAverage;
	public  float scaleAmpVert;
	//----------------------------	
	private int frameBufferID;
	private int textureBufferID;

	public float averagedNormalisedSamples=0.0f;
	private KaleidoscopeEffect Kaleido;
	private VerticalBlurEffect vertBlur;;

	//----------------------------
	 
//-----------------------------------------------------------------------------	
	public OscPlugin(Context context)
	{
		super(context);
		
		final String vertexShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.osc_vs);   		
	 	final String fragmentShader = RawResourceReader.readTextFileFromRawResource(mContext, R.raw.osc_fs);			
			
		final int vertexShaderHandle = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);		
		final int fragmentShaderHandle = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);		
			
		mProgram = ShaderHelper.createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle,new String[] {"a_Position"}); 
			
		// get handle to the vertex shader's member
        maPositionHandle = GLES20.glGetAttribLocation(mProgram, "a_Position");
     
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_MVPMatrix");
        mMVMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_MVMatrix"); 
        // get handle to fragment shader's  member
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        mColorEndHandle = GLES20.glGetUniformLocation(mProgram, "vColorEnd");
        mAveragedSamplesHandle = GLES20.glGetUniformLocation(mProgram, "u_averagedSamples");
        vertexBuffer = ByteBuffer.allocateDirect((sampleSize)*3*FLOAT_SIZE_BYTES).order(ByteOrder.nativeOrder()).asFloatBuffer();
        
     //   Log.i(TAG,"osc constructor");
        
       // setup Frame Buffers
        textureBufferID = createTargetTexture();
        frameBufferID =  createFrameBuffer(textureBufferID);
        
        //add in kaliedoscope and vertical blur contexts for testing
        Kaleido = new KaleidoscopeEffect(mContext);
        vertBlur = new VerticalBlurEffect(mContext);
	}
//----------------------------------------------------------------------------
	public void addFloatVertices(FloatBuffer vertices)
	{
		float averageSamples=0.0f;
		 float sampleNum = 0.0f;
		 scaleAmpVert = 1.0f;

		vertexBuffer.position(0);
		
		//Log.i("frame samples ", String.valueOf(averageSamples) );
		float counter=0.0f;
		
		sampleNum = -400.0f;
		averagedNormalisedSamples =0.0f;
		for(int x=0; x<sampleSize;x++)
		{
			counter = sampleNum / 400.0f;
			vertexBuffer.put (counter );
			vertexBuffer.put(vertices.get(x));
			vertexBuffer.put(0.0f);
			sampleNum++;
		}
	/*	for(int x=0; x<sampleSize/4;x++)
		{
			/*averageSamples = (vertices.get(x)+vertices.get(x+1))/2;
			averageSamples = averageSamples/32768;
			averagedNormalisedSamples += Math.abs(averageSamples);
			counter = sampleNum / 400.0f;
			vertexBuffer.put (counter);
			vertexBuffer.put(averageSamples);
			vertexBuffer.put(0.0f);
			sampleNum++;
			*/
		/*	counter = sampleNum / 400.0f;
			vertexBuffer.put (counter );
			vertexBuffer.put(vertices.get(x));
			vertexBuffer.put(0.0f);
			sampleNum++;
		}
	*/
		//vertexBuffer.position(2997);	
		//vertexBuffer.put (1.0f);
		
		averagedNormalisedSamples = averagedNormalisedSamples/(sampleNum*2.0f);
		vertexBuffer.position(0);
	//	Log.i("frame  sample num ", String.valueOf(counter) );
	//	Log.i("frame samples x ", String.valueOf(vertexBuffer.get(2391)) );
	//	Log.i("frame samples y ", String.valueOf(vertexBuffer.get(2394)) );
	//	Log.i("frame samples z ", String.valueOf(vertexBuffer.get(2397)) );
		
		//vertexBuffer.position(0);
	}
//------------------------------------------------------------------------------------
	public void useAndSendGLSL()
	{
		// Add program to OpenGL environment
	    GLES20.glUseProgram(mProgram);

	    float[] color = {  0.0f, 0.5f, 5.0f, 1.0f};
	    float[] colorEnd = {  1.0f, 1.0f, 0.0f, 1.0f};
		// Set color for drawing the triangle
	   GLES20.glUniform4fv(mColorHandle, 1, color , 0);
	   GLES20.glUniform4fv(mColorEndHandle, 1, colorEnd , 0);
	   
	   // float normalisedScaleFactor = 2.0f;
		//if(averagedNormalised < )
	   // mNormalisedAverage = averagedNormalisedSamples*normalisedScaleFactor ;	
	    	
	
	    GLES20.glUniform1f(mAveragedSamplesHandle, mNormalisedAverage);
	    
	// checkGlError("useAndSendGLSL");
	}
//------------------------------------------------------------------------------------
	public void buildMatrices()
	{ 
		// Set the view matrix. This matrix can be said to represent the camera position.
		Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
		
		Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
		
		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, 0.0f, 0.0f,  -1.5f);

	   // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
	   // (which currently contains model * view).
	   Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);   
		   
	   // Pass in the model view matrix.
	   GLES20.glUniformMatrix4fv(mMVMatrixHandle, 1, false, mMVPMatrix, 0);                
		   
	   // This multiplies the model view matrix by the projection matrix, and stores the result in the MVP matrix
	   // (which now contains model * view * projection).        
	   Matrix.multiplyMM(mTemporaryMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
	   System.arraycopy(mTemporaryMatrix, 0, mMVPMatrix, 0, 16);
		
	   // Pass in the combined matrix.
	   GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);
	  // checkGlError("buildMatrices");
	}	
//------------------------------------------------------------------------------------
	public void drawArray(float averagedNormalised) //float averagedNormalised
	{
		mNormalisedAverage = averagedNormalised;
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		
		//bind frame buffer and draw offscreen into buffer
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frameBufferID);

		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);// | GLES20.GL_DEPTH_BUFFER_BIT);
		
		useAndSendGLSL();

		buildMatrices(); 

		vertexBuffer.position(0);
		GLES20.glVertexAttribPointer(maPositionHandle, 3, GLES20.GL_FLOAT, false,0, vertexBuffer);
		GLES20.glEnableVertexAttribArray(maPositionHandle);
		
//		GLES20.glLineWidth(8.0f);
		GLES20.glLineWidth(4.0f);

		// Draw the triangle
		GLES20.glDrawArrays(GLES20.GL_LINE_STRIP  , 0, sampleSize);
		//GLES20.glDrawArrays(GLES20.GL_TRIANGLES , 0, sampleSize);
		checkGlError("glDrawArrays");
		
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
		
		//---------------------------------
		//draw on screen 
		
		float[] rotateAngleAxisXYZ = {270.0f, 0.0f,0.0f,1.0f};
		float[] translate = {0.0f,0.0f,-4.5f};  //float[] translate = {0.0f,0.0f,-4.5f};
		
		drawPassThruShader(translate,rotateAngleAxisXYZ); 
	//	Kaleido.drawKaleidoscopeShader(textureBufferID, mQuadBufferIdx, averagedNormalised);
//		vertBlur.drawVerticalBlurShader(textureBufferID, mQuadBufferIdx, averagedNormalised);
		//---------------------------------
		
	//	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
	//	GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
	}
//----------------------------


}
