precision mediump float;
uniform vec2 u_texdim0; 			//texture dimension 800,480
uniform vec2 u_scale;				//u_scale of texture in folds 1
uniform vec2 u_offset;			//u_offset of pixels into texture 0
uniform vec2 u_origin;			//u_origin of starting texture 0
uniform float u_div;  			//number of u_divisions or folds 4
uniform float u_divamount;		//amount of pixels per u_division 1
uniform sampler2D u_Texture;	
varying vec3 v_Position;		// These will be passed into the fragment shader
varying vec2 v_TexCoordinate;

const float pi=3.1415926;

void main()
{
	vec2 point = abs(mod(((v_TexCoordinate*u_scale)/u_texdim0+ u_origin),1.0));
	vec2 normalizedCoords = vec2(2.0) * point - vec2(1.0);

	float theta =  (pi) + atan(normalizedCoords.x,normalizedCoords.y) * (u_divamount);

	float radius = sqrt(dot(normalizedCoords, normalizedCoords));

	float phi = 2.*pi/u_div;

	float modtheta = mod(abs(theta),phi*2.);
	float foldtheta = phi-abs(modtheta-phi);

	vec2 no = vec2(-cos(foldtheta)*radius, -sin(foldtheta)*radius);	
	no = (no+0.5)*u_texdim0;

	vec2 off = abs(no+u_offset*u_texdim0);
	vec2 modoff = mod(off,u_texdim0*2.);
	vec2 fold = u_texdim0-abs(modoff-u_texdim0);
	vec4 baseColor = texture2D(u_Texture, fold);
 	if(baseColor.rgb == vec3(0.0))
    	baseColor.a=0.0;
	gl_FragColor = baseColor; // v_TexCoordinate fold
}