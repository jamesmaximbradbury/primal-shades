precision mediump float;
#define NUMWT 9
float Gauss[NUMWT];
//#define WT_NORMALIZE (1.0/(1.0+2.0*(0.93 + 0.8 + 0.7 + 0.6 + 0.5 + 0.4 + 0.3 + 0.2 + 0.1)))
#define WT_NORMALIZE 0.5
uniform sampler2D u_Texture;
uniform vec2 u_blurY;
varying vec2 v_TexCoordinate;   // Interpolated texture coordinate per fragment
varying vec3 v_Position;		// Interpolated position for this fragment.

void main()
{
  Gauss[0]=0.93; Gauss[1]=0.8; Gauss[2]=0.7; Gauss[3]=0.6; Gauss[4]=0.5; Gauss[5]=0.4; Gauss[6]=0.3;Gauss[7]=0.2; Gauss[8]=0.1; 

  vec4 c2;
  vec4 c = texture2D(u_Texture,  v_TexCoordinate.xy)* (WT_NORMALIZE);
  vec2 step = u_blurY;
  vec2 dir = step;
  for(int i=0; i<2; i++) //number of iterations max's out at 7
  {
	c2 = texture2D(u_Texture,  v_TexCoordinate.xy + dir);
	c += c2 * (Gauss[i]*WT_NORMALIZE);
	c2 = texture2D(u_Texture,  v_TexCoordinate.xy - dir);
	c += c2 * (Gauss[i]*WT_NORMALIZE);
	dir += step;
  }
  if(c.rgb == vec3(0.0))
    	c.a=0.0;
	gl_FragColor = c;
}
