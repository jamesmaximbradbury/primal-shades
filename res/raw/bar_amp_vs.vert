uniform mat4 u_MVMatrix;      	
uniform mat4 u_MVPMatrix;		// A constant representing the combined model/view/projection matrix.
uniform float u_Amp;	
attribute vec4 a_Position;     		
attribute vec2 a_TexCoordinate;  // Per-vertex texture coordinate information we will pass in. 
varying vec2 v_TexCoordinate; // This will be passed into the fragment shader.  
varying vec3 v_Position;		// This will be passed into the fragment shader.    

void main()                    
{                  

// Transform the vertex into eye space. 	
	v_Position =vec3( a_Position);//vec3(u_MVMatrix * a_Position); 

    // Pass through the texture coordinate.
    v_TexCoordinate = a_TexCoordinate;       
     
    vec3 newpos;
    
    newpos = vec3(a_Position);
    
    if(newpos.y > 0.1)
    	newpos.y = newpos.y * u_Amp - newpos.y;
   
    
    newpos.x *= 0.1; //0.2
    newpos.z *= 0.05;
                  
	gl_Position = u_MVPMatrix * vec4(newpos,1.0);   
	
/*	v_Position = vec3(u_MVMatrix * a_Position); 

    // Pass through the texture coordinate.
    v_TexCoordinate = a_TexCoordinate;       
                   
	gl_Position = u_MVPMatrix * a_Position;
*/        
}                              