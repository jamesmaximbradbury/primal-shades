uniform mat4 u_MVMatrix;      	
uniform mat4 u_MVPMatrix;		// A constant representing the combined model/view/projection matrix.	
attribute vec4 a_Position;     		
//attribute vec4 vPosition;
attribute vec2 a_TexCoordinate;  // Per-vertex texture coordinate information we will pass in. 
varying vec2 v_TexCoordinate; // This will be passed into the fragment shader.  
varying vec3 v_Position;		// This will be passed into the fragment shader.    

void main()                    
{                  
// Transform the vertex into eye space. 	
	v_Position = vec3(u_MVMatrix * a_Position); 

    // Pass through the texture coordinate.
    v_TexCoordinate = a_TexCoordinate;       
    //gl_Position = a_Position;                   
	gl_Position = u_MVPMatrix * a_Position;   
   	// gl_PointSize = 5.0;         
}                              