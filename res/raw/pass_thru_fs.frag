precision mediump float;
//uniform vec4 vColor; 
uniform sampler2D u_Texture;   // The input texture.
varying vec2 v_TexCoordinate;   // Interpolated texture coordinate per fragment
varying vec3 v_Position;		// Interpolated position for this fragment.
void main(){             
    vec4 baseColor = texture2D(u_Texture, v_TexCoordinate); 
    if(baseColor.rgb == vec3(0.0))
    	baseColor.a=0.0;
    gl_FragColor = baseColor;   //vec4(0.0,0.7,0.7,baseColor.w);//
}                  