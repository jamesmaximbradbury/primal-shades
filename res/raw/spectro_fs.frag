precision mediump float;
uniform vec4 vColor; 
uniform float u_averagedSamples; 
uniform sampler2D u_Texture;   // The input texture.
varying vec2 v_TexCoordinate;   // Interpolated texture coordinate per fragment
varying vec3 v_Position;		// Interpolated position for this fragment.
void main(){           
  
    vec4 baseColor = texture2D(u_Texture, v_TexCoordinate); 
	//baseColor = vec4(baseColor.g*abs(v_Position.y));
	baseColor = vec4(abs(v_Position.y)*baseColor.r, abs(v_Position.y)*baseColor.g, abs(v_Position.y)*baseColor.b, 1.0);
	
    vec3 RGB2luminance = vec3 (0.2125, 0.7154, 0.0721);

    float luminance = dot(RGB2luminance, baseColor.rgb);
	
	//gl_FragColor = vec4(0.0,0.5,0.5,1.0);
	
		vec3 newLumCol = vec3(0.0);
		
					if(luminance>0.05)
					{
						newLumCol = baseColor.rgb * u_averagedSamples;
						if ( newLumCol.r < baseColor.r )
							gl_FragColor = baseColor;
						else
						{
							gl_FragColor.rgb  = newLumCol;
							gl_FragColor.a = 1.0;
						}
					}
					else
						gl_FragColor = baseColor;
	
}                  