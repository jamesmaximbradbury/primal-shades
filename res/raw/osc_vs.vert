uniform mat4 u_MVMatrix;      	
uniform mat4 u_MVPMatrix;		// A constant representing the combined model/view/projection matrix.	
attribute vec4 a_Position;     		 
varying vec4 v_Position;		// This will be passed into the fragment shader.    

void main()                    
{                  
// Transform the vertex into eye space. 	
//	v_Position = vec3(u_MVMatrix * a_Position); 
	
	vec3 newpos = vec3(a_Position);
	newpos.y = newpos.y * 0.9;
    
    // Pass through the texture coordinate.
  //  v_TexCoordinate = a_TexCoordinate;       
                     
	gl_Position = u_MVPMatrix * a_Position; 
	
	v_Position = gl_Position ;          	
}                              