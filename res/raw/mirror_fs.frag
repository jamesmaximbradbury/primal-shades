precision mediump float;
uniform sampler2D u_Texture;							//¬´u_Texture¬´sampler2DRect¬´0¬´0¬´0¬´NO¬´
uniform vec2 u_texdim0;	//this changes mirror amount	//¬´u_texdim0¬´vec2¬´720.0¬´0.0¬´2000.0¬´YES¬´
uniform vec2 u_origin;	//is this really the u_origin?	//¬´u_origin¬´vec2¬´0.0¬´-10.0¬´10.0¬´YES¬´
uniform float u_rotationAngle;							//¬´u_rotationAngle¬´float¬´0.0¬´-3.14¬´3.14¬´YES¬´
uniform vec2 u_rotationAnchor;							//¬´u_rotationAnchor¬´vec2¬´0.0¬´-3.0¬´3.0¬´YES¬´
uniform vec2 u_zAnchor;									//¬´u_zAnchor¬´vec2¬´0.0¬´-3.0¬´3.0¬´YES¬´
uniform float u_zoom;										//¬´zoom¬´float¬´0.0¬´-5.0¬´5.0¬´YES¬´
uniform float u_zoomMirror;								//¬´zoomMirror¬´float¬´0.0¬´-5.0¬´5.0¬´YES¬´
varying vec2 v_TexCoordinate;   // Interpolated texture coordinate per fragment
varying vec3 v_Position;		// Interpolated position for this fragment.

void main (void) 
{ 
	//vec2 u_texdim0 = vec2(v_TexCoordinate);
		
	vec2 point = v_TexCoordinate.xy/u_texdim0;
		
	vec2 normCoord = vec2(2.0) * point - vec2(1.0); // coordinates are now -1.0 to 1.0
	
	mat2 rotmat = mat2 (cos(u_rotationAngle),sin(u_rotationAngle),-sin(u_rotationAngle),cos(u_rotationAngle));		
	normCoord = ((normCoord - u_rotationAnchor) * rotmat) + u_rotationAnchor;

	normCoord = vec2((normCoord.x - u_zAnchor.x)/u_zoom + u_zAnchor.y, (normCoord.y - u_zAnchor.y)/u_zoom + u_zAnchor.y); 

	if(all(lessThanEqual(u_origin, vec2(0.0))))
	{
		normCoord = u_origin + abs(normCoord - u_origin);
	}
	else if (u_origin.x < 0.0 && u_origin.y > 0.0)
	{
		normCoord.x= u_origin.x + abs(normCoord.x - u_origin.x);
		normCoord.y= u_origin.y - abs(normCoord.y - u_origin.y);
		
	}
	else if (u_origin.x > 0.0 && u_origin.y < 0.0)
	{
		normCoord.x= u_origin.x - abs(normCoord.x - u_origin.x);
		normCoord.y= u_origin.y + abs(normCoord.y - u_origin.y);
	
	}
	else
	{
		normCoord = u_origin - abs(normCoord - u_origin);
	}

	normCoord = normCoord /u_zoomMirror;

	vec2 texCoord = (normCoord / 2.0 + 0.5) * u_texdim0; // unnormalize
	
	vec4 baseColor = texture2D(u_Texture, texCoord);
	
	if(baseColor.rgb == vec3(0.0))
    	baseColor.a=0.0;
    	
	gl_FragColor = baseColor;

} 