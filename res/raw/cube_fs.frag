precision mediump float;
uniform vec4 vColor; 
uniform float u_averagedSamples; 
uniform sampler2D u_Texture;   // The input texture.
varying vec2 v_TexCoordinate;   // Interpolated texture coordinate per fragment
varying vec3 v_Position;		// Interpolated position for this fragment.
void main(){           
  
    vec4 baseColor = texture2D(u_Texture, v_TexCoordinate); 

    vec3 RGB2luminance = vec3 (0.2125, 0.7154, 0.0721);

    float luminance = dot(RGB2luminance, baseColor.rgb);
/*
	float dist = 0.01; //u_averagedSamples/100;

	//if a dark spot search for some light spots and if 
	//it finds some light spots in the distance make this
	//dark spot a light spot	
	
	bool dobreak = false;
	if(luminance<0.5) 
	{
		for(float x =- dist; !dobreak && x <= dist; x+=0.002)
		{
			for(float y = -dist; y <= dist; y+=0.002)
			{
 				 vec2 searchCoord = vec2(v_TexCoordinate.x+x,v_TexCoordinate.y+y);
				 vec4 searchColor = texture2D(u_Texture, searchCoord);
				 float searchLuminance = dot(RGB2luminance, searchColor.rgb);
				 if(searchLuminance>0.5)
				 {
				 	gl_FragColor  = searchColor;//searchColor; vec4(0.0,0.0,0.0,1.0)	
				 	dobreak =true;
				 	break;
				 }
				 else
				 {
				 	vec3 newLumCol = vec3(0.0);
		
					if(luminance>0.05)
					{
						newLumCol = baseColor.rgb * u_averagedSamples;
						if ( newLumCol.r < baseColor.r )
							gl_FragColor = baseColor;
						else
							gl_FragColor.rgb  = newLumCol;
					}
					else
						gl_FragColor = baseColor;
				 	
			 	 }
			}
		}
			
	}
	else
		gl_FragColor = baseColor;		
*/		
		vec3 newLumCol = vec3(0.0);
	//	if(baseColor.r>0.2)
		//	baseColor.rgb = baseColor.rgb + vec3(0.0,0.6,0.8);
					if(luminance>0.05)
					{
						newLumCol = baseColor.rgb * u_averagedSamples;
						if ( newLumCol.r < baseColor.r )
							gl_FragColor = baseColor;
						else
							gl_FragColor.rgb  = newLumCol;
					}
					else
						gl_FragColor = baseColor;
}                  