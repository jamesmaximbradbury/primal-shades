precision mediump float;
uniform vec4 vColor; 
uniform vec4 vColorEnd; 
uniform float u_averagedSamples; 
//uniform sampler2D u_Texture;   // The input texture.
//varying vec2 v_TexCoordinate;   // Interpolated texture coordinate per fragment
varying vec4 v_Position;		// Interpolated position for this fragment.
void main(){             
  	//  vec4 baseColor = texture2D(u_Texture, v_TexCoordinate); 

   vec4 modColour = vec4(abs(v_Position.x)*vColorEnd.r, abs(v_Position.y)*vColorEnd.g, abs(v_Position.z)*vColorEnd.b, 1.0);
	
	vec4 baseColor = vColor+modColour;
	
	if(u_averagedSamples<0.45)
	   	baseColor.g = v_Position.x+modColour.g;
/*
if(u_averagedSamples>0.35)
	   	baseColor.r = v_Position.x+modColour.r;
*/
	
	if(u_averagedSamples>0.6)
		baseColor.rgb = vec3(1.0,0.0,0.0);
//   	baseColor.rgb = vColor.rgb+vec3(u_averagedSamples);
 	
 	if(baseColor.rgb == vec3(0.0))
    	baseColor.a=0.0;
    
    gl_FragColor = baseColor; //baseColor;   
}                  