precision mediump float;
uniform sampler2D u_Texture;
//uniform sampler2D Noise2DSamp;
//uniform  vec4 vecSkill1;
//uniform  vec4 vecTime;
uniform  vec2 vecSkill; // dft 0.0 max 20 max.y 0.2
uniform  float vecTime; //dft 0 max 2
varying vec2 v_TexCoordinate;   // Interpolated texture coordinate per fragment
varying vec3 v_Position;	

void main()
{
	//vec4 Color;//=texture2D(u_Texture, gl_TexCoord[0].xy);
	vec2 Tex = v_TexCoordinate.xy;
	Tex.y += (sin(Tex.x*vecTime*vecSkill.x)*vecSkill.y);
	
	vec4 baseColor = texture2D( u_Texture, Tex.xy);
 	if(baseColor.rgb == vec3(0.0))
    	baseColor.a=0.0;
	gl_FragColor = baseColor;
	//gl_FragColor = Color ;
	
}

//gl_Color * texture2DRect(u_Texture,v_TexCoordinate.xy);